<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jasa extends Model
{
    //
    protected $table = 'jasa';

	protected $fillable = [
        'user_id', 'name', 'verified',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->hasMany('App\Product', 'jasa_id')->where('status', '<>', 4);
    }

    public function order()
    {
        return $this->hasManyThrough('App\Order', 'App\Product', 'jasa_id', 'products_id');
    }
}
