<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Order extends Model
{
    //
    use HasRelationships;
    protected $table = 'orders';

    protected $fillable = [
        'products_id', 'user_id', 'status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'products_id');
    }

    public function payment()
    {
        return $this->hasOne('App\Payment', 'order_id');
    }

    public function rating()
    {
        return $this->hasOne('App\Rating', 'order_id');
    }
}
