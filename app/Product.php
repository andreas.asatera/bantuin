<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 */
class Product extends Model
{
    //
    protected $table = 'products';

    protected $fillable = [
        'jasa_id', 'name', 'price', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function jasa()
    {
        return $this->belongsTo('App\Jasa')->where('verified', 1);
    }

    public function order()
    {
        return $this->hasMany('App\Order', 'products_id');
    }

    public function rating()
    {
        return $this->hasManyThrough('App\Rating', 'App\Order', 'products_id', 'order_id');
    }

    public function favorite(){
        return $this->hasMany('App\Favorite', 'products_id');
    }
}
