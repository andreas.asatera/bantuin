<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyJasaAdmin extends Notification
{
    use Queueable;
    public $jasa;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($jasa)
    {
        //
        $this->jasa = $jasa;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Verifikasi Jasa')
            ->markdown('emails.verifyjasa', ['jasa' => $this->jasa]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'message' => '[ADMIN]Ada user yang menambahkan jasa baru',
            'url' => route('admin-jasa-review', ['id'=>$this->jasa->id])."?notif_id=$this->id"
        ];
    }
}
