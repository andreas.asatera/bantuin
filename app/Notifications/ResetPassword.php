<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPassword extends ResetPasswordNotification
{
    use Queueable;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        //
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = [
            'greeting' => 'Hi',
            'body' => 'Anda menerima email ini karena kami menerima permintaan setel ulang kata sandi untuk akun anda. Silahkan klik dibawah untuk menyetel ulang kata sandi akun anda',
            'thanks' => 'Terima Kasih.',
            'actionText' => 'Setel Ulang Kata Sandi',
            'actionURL' => url('password/reset', $this->token),
        ];
        return (new MailMessage)
            ->subject('Setel Ulang Kata Sandi')
            ->greeting($message['greeting'])
            ->line($message['body'])
            ->action($message['actionText'], $message['actionURL'])
            ->line('Abaikan emal ini jika anda tidak mengirimkan permintaan untuk reset password.')
            ->line($message['thanks']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
