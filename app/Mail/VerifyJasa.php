<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyJasa extends Mailable
{
    use Queueable, SerializesModels;
    public $jasa;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($jasa)
    {
        //
        $this->jasa = $jasa;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.verifyjasa');
    }
}
