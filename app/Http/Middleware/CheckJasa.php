<?php

namespace App\Http\Middleware;

use Closure;

class CheckJasa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset($request->user()->jasa)) {
            return redirect()->route('user-jasa');
        }
        return $next($request);
    }
}
