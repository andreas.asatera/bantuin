<?php

namespace App\Http\Middleware;

use Closure;

class CheckIdentitas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset($request->user()->noidentitas) || !isset($request->user()->fotoidentitas)) {
            return redirect()->route('user')->with('error', 'Kamu belum menambahkan identitas');
        }
        elseif (isset($request->user()->statusidentitas) && $request->user()->statusidentitas != 2){
            return redirect()->route('user')->with('error', 'Identitas belum diverifikasi oleh admin');
        }
        return $next($request);
    }
}
