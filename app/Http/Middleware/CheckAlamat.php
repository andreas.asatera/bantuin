<?php

namespace App\Http\Middleware;

use Closure;

class CheckAlamat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset($request->user()->provinsi) || !isset($request->user()->kabkota) || !isset($request->user()->kecamatan) || !isset($request->user()->desa) || !isset($request->user()->alamat)) {
            return redirect()->route('user')->with('error', 'Kamu belum menambahkan alamat');
        }
        return $next($request);
    }
}
