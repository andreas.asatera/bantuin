<?php

namespace App\Http\Controllers;

use App\Mail\VerifyPayment;
use App\Payment;
use File;
use Illuminate\Http\Request;
use Image;
use Storage;
use Validator;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $payment = Payment::where('order_id', $request->orderid)->first();
        $payment->nama = $request->nama;
        $payment->norek = $request->norek;
        $payment->status = 2;
        $validator = Validator::make($request->all(),
            [
                'bukti' => 'required|image|mimes:jpeg,png,jpg,gif|max:5024',
            ],
            [
                'bukti.mimes' => 'Upload foto profil menggunakan format jpeg/jpg/png/gif',
                'bukti.max' => 'Ukuran maksimal foto profil yaitu 5024 KB',
            ]);
        if ($validator->passes()) {
            if (isset($payment->bukti)){
                File::delete('images/payment/' . $payment->bukti);
            }
            $image = Image::make($request->file('bukti'));
            $filename = rand() . '.' . $request->file('bukti')->getClientOriginalExtension();
            $image->stream();
            Storage::disk('public_upload')->put('images/payment/' . $filename, $image, 'public');
            $payment->bukti = $filename;
            $payment->save();
            \Mail::to(config('mail.from.address'))->send(new VerifyPayment($payment));
            return redirect()->back()->with('status', 1);
        }
        else{
            return redirect()->back()->with('error', 1);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
