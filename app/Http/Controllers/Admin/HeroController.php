<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $heroes = User::whereNotNull('hero')->get();

        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('admin.hero.index')->with([
                'user' => $user,
                'heroes' => $heroes
            ]);
        }

        return view("admin.hero.index", [
            'user' => $user,
            'heroes' => $heroes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();
        $userView = User::find($id);
        return view("admin.hero.review", [
            'user' => $user,
            'userView' => $userView
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        if ($request->status == 1){
            $user->hero = 2;
            $message = [
                'greeting' => 'Hi '.$user->name,
                'body' => 'Verifikasi identitas anda telah berhasil. Silahkan klik dibawah untuk menuju profil anda',
                'short' => 'Verifikasi identitas anda telah berhasil',
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
//            $user->notify(new VerifyId($message));
        }
        if ($request->status == 2){
            $user->hero = null;
            $message = [
                'greeting' => 'Hi '.$user->name,
                'body' => "Maaf, verifikasi identitas kamu di tolak oleh admin dengan alasan: <b>".$request->alasan."</b>. Silahkan klik dibawah untuk menuju profil anda",
                'short' => "Maaf, verifikasi identitas kamu di tolak oleh admin dengan alasan: <b>$request->alasan</b>",
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
//            $user->notify(new VerifyId($message));
        }
        $user->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
