<?php

namespace App\Http\Controllers\Admin;

use App\User;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Notifications\VerifyId;

class UserController extends Controller
{
    //
    public function index()
    {
        //
        $user = Auth::user();
        $userList = User::all();
        return view("admin.home.user", [
            'user' => $user,
            'userList' => $userList
        ]);
    }
    public function show($id)
    {
        //
        $user = Auth::user();
        $userView = User::find($id);
        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('admin-user-review', ['id'=>$userView])->with([
                'user' => $user,
                'userView' => $userView
            ]);
        }
        return view("admin.user.review", [
            'user' => $user,
            'userView' => $userView
        ]);
    }
    public function verification(Request $request){
        $user = User::find($request->id);
        if ($request->status == 1){
            $user->statusidentitas = 2;
            $user->alasan = null;
            $message = [
                'greeting' => 'Hi '.$user->name,
                'body' => 'Verifikasi identitas anda telah berhasil. Silahkan klik dibawah untuk menuju profil anda',
                'short' => 'Verifikasi identitas anda telah berhasil',
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
            $user->notify(new VerifyId($message));
        }
        if ($request->status == 2){
            $user->noidentitas = null;
            File::delete('/images/identitas/' . $user->fotoidentitas);
            $user->fotoidentitas = null;
            $user->statusidentitas = null;
            $user->alasan = $request->alasan;
            $message = [
                'greeting' => 'Hi '.$user->name,
                'body' => "Maaf, verifikasi identitas kamu di tolak oleh admin dengan alasan: <b>".$request->alasan."</b>. Silahkan klik dibawah untuk menuju profil anda",
                'short' => "Maaf, verifikasi identitas kamu di tolak oleh admin dengan alasan: <b>$request->alasan</b>",
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
            $user->notify(new VerifyId($message));
        }
        $user->save();
        return redirect()->back();
    }
}
