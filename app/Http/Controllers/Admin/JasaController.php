<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\VerifyJasa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Jasa;

class JasaController extends Controller
{
    //
    public function index()
    {
        //
        $user = Auth::user();
        $jasa = Jasa::with('user')->get();
        return view("admin.jasa.jasa", [
            'user' => $user,
            'jasaList' => $jasa
        ]);
    }

    public function review($id)
    {
        //
        $user = Auth::user();
        $jasa = Jasa::with('user')->where('id', $id)->first();
        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('admin-jasa-review', ['id'=>$jasa])->with([
                'user' => $user,
                'jasa' => $jasa
            ]);
        }
        return view("admin.jasa.reviewjasa", [
            'user' => $user,
            'jasa' => $jasa
        ]);
    }

    public function reviewpost(Request $request, $id)
    {
        //
        $jasa = Jasa::with('user')->where('id', $id)->first();
        switch ($request->status) {
            case 1: // approve
                $jasa->verified = 1;
                $jasa->alasan = null;
                $jasa->save();
                $message = [
                    'subject' => 'Verifikasi Jasa',
                    'greeting' => 'Hi '.$jasa->user->name,
                    'body' => 'Verifikasi jasa anda telah berhasil. Silahkan klik dibawah untuk menuju halaman jasa anda',
                    'short' => 'Verifikasi jasa anda telah berhasil',
                    'thanks' => 'Terima Kasih.',
                    'actionText' => 'Buka Jasa',
                    'actionURL' => route('user-jasa'),
                ];
                $jasa->user->notify(new VerifyJasa($message));
                session()->flash('status', 'success');
                session()->flash('message', 'Approval <b>' . $jasa->name . '</b> sukses');
                return redirect()->route('admin-jasa');
                break;
            case 2: // ban
                $jasa->verified = 2;
                $jasa->save();
                $message = [
                    'subject' => 'Bantuin Jasa',
                    'greeting' => 'Hi '.$jasa->user->name,
                    'body' => 'Jasa anda telah di blokir oleh admin. Silahkan hubungi admin untuk informasi lebih lanjut',
                    'short' => 'Jasa anda telah di blokir oleh admin',
                    'thanks' => 'Terima Kasih.',
                    'actionText' => 'Buka Jasa',
                    'actionURL' => route('user-jasa'),
                ];
                $jasa->user->notify(new VerifyJasa($message));
                session()->flash('status', 'danger');
                session()->flash('message', 'Approval <b>' . $jasa->name . '</b> dicabut');
                return redirect()->route('admin-jasa');
                break;
            case 3: // tolak
                $jasa->verified = 3;
                $jasa->alasan = $request->alasan;
                $jasa->save();
                $message = [
                    'subject' => 'Verifikasi Jasa',
                    'greeting' => 'Hi '.$jasa->user->name,
                    'body' => "Verifikasi jasa anda ditolak oleh admin dengan alasan: <b>$request->alasan</b>. Silahkan klik dibawah untuk menuju halaman jasa anda",
                    'short' => "Verifikasi jasa anda ditolak oleh admin dengan alasan: <b>$request->alasan</b>",
                    'thanks' => 'Terima Kasih.',
                    'actionText' => 'Buka Jasa',
                    'actionURL' => route('user-jasa'),
                ];
                $jasa->user->notify(new VerifyJasa($message));
                session()->flash('status', 'danger');
                session()->flash('message', 'Pengajuan jasa <b>' . $jasa->name . '</b> ditolak');
                return redirect()->route('admin-jasa');
                break;
            case 4: // cancel
                session()->flash('status', 'danger');
                session()->flash('message', 'Approval <b>' . $jasa->name . '</b> dibatalkan');
                return redirect()->route('admin-jasa');
                break;
            default:
                break;
        }
    }
}
