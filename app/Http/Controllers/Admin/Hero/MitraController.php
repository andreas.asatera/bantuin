<?php

namespace App\Http\Controllers\Admin\Hero;

use App\Jasa;
use App\Notifications\VerifyJasa;
use App\Notifications\VerifyJasaAdmin;
use App\User;
use Auth;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $mitras = User::whereNotNull('ref')->get();
        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('admin.hero.mitra.index')->with([
                'user' => $user,
                'mitras' => $mitras
            ]);
        }

        return view("admin.hero.mitra.index", [
            'user' => $user,
            'mitras' => $mitras
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();
        $userView = User::find($id);
        return view("admin.hero.mitra.review", [
            'user' => $user,
            'userView' => $userView
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        $jasa = Jasa::find($user->jasa->id);
        if ($request->status == 1){
            $user->statusidentitas = 2;
            $user->alasan = null;
            $jasa->verified = 1;
            $jasa->alasan = null;
            $jasa->save();
            $message = [
                'subject' => 'Verifikasi Mitra',
                'greeting' => 'Hi '.$user->name,
                'body' => 'Verifikasi mitra telah berhasil. Silahkan klik dibawah untuk menuju profil anda',
                'short' => 'Verifikasi mitra telah berhasil',
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
            $user->notify(new VerifyJasa($message));
        }
        if ($request->status == 2){
            $jasa->verified = 3;
            $jasa->alasan = $request->alasan;
            $jasa->save();
            $user->noidentitas = null;
            File::delete('/images/identitas/' . $user->fotoidentitas);
            $user->fotoidentitas = null;
            $user->statusidentitas = null;
            $user->alasan = $request->alasan;
            $message = [
                'subject' => 'Verifikasi Mitra',
                'greeting' => 'Hi '.$user->name,
                'body' => "Maaf, verifikasi mitra di tolak oleh admin dengan alasan: <b>".$request->alasan."</b>. Silahkan klik dibawah untuk menuju profil anda",
                'short' => "Maaf, verifikasi mitra di tolak oleh admin dengan alasan: <b>$request->alasan</b>",
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Profil',
                'actionURL' => route('user'),
            ];
            $user->notify(new VerifyJasa($message));
        }
        $user->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
