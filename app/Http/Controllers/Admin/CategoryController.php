<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $category = Category::doesntHave('parent')->get();
        $parent = Category::has('children')->get();
        $children = Category::has('parent')->get();
        return view("admin.home.category", ['user' => $user, 'category' => $category, 'parent' => $parent, 'children' => $children]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = new Category;
        $category->name = $request->nama;
        if ($request->has('id')) {
            $category->parent_id = $request->id;
        }
        $category->save();
        return redirect()->back()->with('status', 'Sukses menambahkan kategori/sub kategori: '. $category->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        $user = Auth::user();
        $category = Category::all();
        $parent = Category::doesntHave('parent')->get();
        return view("admin.home.editcategory", ['user' => $user, 'categories' => $category, 'parentList' => $parent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $category = Category::find($request->cid);
        $category->name = $request->name;
        if ($request->jenis != 0) {
            $category->parent_id = $request->jenis;
        }
        $category->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
