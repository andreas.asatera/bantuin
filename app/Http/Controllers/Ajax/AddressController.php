<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    //
    public function provinsi($id){
        return \Indonesia::findProvince($id, ['cities']);
    }
    public function kabkota($id){
        return \Indonesia::findCity($id, ['districts']);
    }
    public function kecamatan($id){
        return \Indonesia::findDistrict($id, ['villages']);
    }
    public function store(Request $request){
        $user = \Auth::user();
        $user->provinsi = $request->provinsi;
        $user->kabkota = $request->kabkota;
        $user->kecamatan = $request->kecamatan;
        $user->desa = $request->desa;
        $user->alamat = $request->alamat;
        $user->save();
        return redirect()->back();
    }
}
