<?php

namespace App\Http\Controllers\Order;

use App\Notifications\OrderNotification;
use App\Order;
use App\Payment;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        //
        $user = Auth::user();
        $order = Order::whereHas('product.jasa.user', function ($q){
            $q->where('id', Auth::id());
        })->get();

        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('order')->with([
                'user' => $user,
                'orders' => $order
            ]);
        }

        return view('order.index', [
            'user' => $user,
            'orders' => $order
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $order = Order::find($request->id);
        if ($request->has('terima')){
            $order->status = 2;
            $order->save();

            $payment = new Payment();
            $payment->order_id = $request->id;
            $payment->user_id = $order->user_id;
            $payment->nominal = $order->product->price + rand(100, 999);
            $payment->save();

            $message = [
                'subject' => 'Pesanan Jasa Diterima.',
                'greeting' => 'Hi '.$order->user->name,
                'body' => 'Pesanan jasa anda diterima oleh Penyedia Jasa. Silahkan klik tombol dibawah untuk menuju halaman pesanan jasa dan lakukan pembayaran',
                'short' => 'Pesanan jasa anda diterima oleh Penyedia Jasa',
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Pesanan',
                'actionURL' => route('user-order'),
            ];
            $order->user->notify(new OrderNotification($message));
        }
        if ($request->has('tolak')){
            $order->status = 3;
            $order->save();
        }
        return redirect()->back();
    }

    public function done(Request $request)
    {
        //
        $order = Order::find($request->id);
        if ($request->has('selesai')){
            $order->status = 4;
            $order->time_finish = Carbon::now();
            $order->save();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
