<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'nohp' => ['required', 'min:9', 'unique:users'],
            'birthday' => ['required'],
            'gender' => ['required'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ],
            [
                'name.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'nohp.required' => 'Kolom nomor hp harus di isi',
                'birthday.required' => 'Kolom tanggal ulang tahun harus di isi',
                'gender.required' => 'Kolom gender harus di isi',
                'password.required' => 'Kolom password harus di isi',
                'email.unique' => 'Email sudah digunakan',
                'nohp.unique' => 'Nomor HP sudah digunakan'
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'nohp' => $data['nohp'],
            'birthday' => date('Y-m-d', strtotime($data['birthday'])),
            'gender' => $data['gender'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
