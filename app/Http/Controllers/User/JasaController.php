<?php

namespace App\Http\Controllers\User;

use App\Jasa;
use App\Category;
use App\Mail\VerifyJasa;
use App\Notifications\VerifyJasaAdmin;
use App\User;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Facades\Validator;
use Image;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        //
        $user = Auth::user();
        $jasa = Jasa::whereHas('user', function ($q) {
            $q->where('id', '=', Auth::id());
        })->first();
        $category = Category::whereNull('parent_id')->get();
        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('user-jasa')->with([
                'user' => $user,
                'jasa' => $jasa,
                'categoryList' => $category
            ]);
        }
        return view('user.jasa', ['user' => $user, 'jasa' => $jasa, 'categoryList' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if ($request->has('id')) {
            $jasa = Jasa::find($request->id);
            $jasa->verified = 0;
        } else {
            $jasa = new Jasa;
        }
        $jasa->user_id = Auth::id();
        $jasa->name = $request->jasa;
        $jasa->deskripsi = $request->deskripsi;
        $jasa->provinsi = $request->provinsi;
        $jasa->kabkota = $request->kabkota;
        $jasa->kecamatan = $request->kecamatan;
        $jasa->desa = $request->desa;
        $jasa->alamat = $request->alamat;
        $validator = Validator::make($request->all(), [
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',
        ]);
        if ($validator->passes()) {
            if ($request->has('id')) {
                $images = explode("|", $jasa->image);
                foreach ($images as $image) {
                    File::delete('/images/jasa/' . $image);
                }
            }
            foreach ($request->file('image') as $imagefile) {
                $image = Image::make($imagefile);
                $filename = rand() . '.' . $imagefile->getClientOriginalExtension();
                $image->stream();
                Storage::disk('public_upload')->put('images/jasa/' . $filename, $image, 'public');
                $imagename[] = $filename;
            }
            $jasa->image = implode("|", $imagename);
        }
        $jasa->save();
        \Notification::send(User::where('level', '3')->get(), new VerifyJasaAdmin($jasa));
        return redirect()->back();
    }

    public function imgupload(Request $request)
    {
        foreach ($request->file('file') as $imagefile) {
            $image = Image::make($imagefile);
            $filename = rand() . '.' . $imagefile->getClientOriginalExtension();
            $image->stream();
            Storage::disk('public_upload')->put('images/jasa/' . $filename, $image, 'public');
            $imagename[] = $filename;
        }
        return response()->json(['success'=>$imagename]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function show(Jasa $jasa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function edit(Jasa $jasa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jasa $jasa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jasa $jasa)
    {
        //
    }
}
