<?php

namespace App\Http\Controllers\User\Hero;

use App\Jasa;
use App\Notifications\VerifyHeroMitra;
use App\User;
use Auth;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'noidentitas' => 'nullable|string|unique:users,noidentitas,' . $request->customer,
                'fotoidentitas' => 'mimes:jpeg,png,jpg,gif|max:2048',
                'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',
            ],
            [
                'noidentitas.unique' => 'No identitas sudah digunakan',
                'fotoidentitas.mimes' => 'Upload foto identitas menggunakan format jpeg/jpg/png/gif',
                'fotoidentitas.max' => 'Ukuran maksimal foto identitas yaitu 2048 KB',
                'image.mimes' => 'Upload foto jasa menggunakan format jpeg/jpg/png/gif',
                'image.max' => 'Ukuran maksimal foto jasa yaitu 5120 KB',
            ]);
        if ($validator->passes()) {
            $user = User::find($request->customer);

            /* No Identitas*/
            $user->noidentitas = $request->noidentitas;

            /* Foto Identitas */
            File::delete('/images/identitas/' . $user->fotoidentitas);
            $image = Image::make($request->file('fotoidentitas'));
            $filename = rand() . '.' . $request->file('fotoidentitas')->getClientOriginalExtension();
            $image->stream();
            Storage::disk('public_upload')->put('images/identitas/' . $filename, $image, 'public');
            $user->fotoidentitas = $filename;

            /* Status Identitas */
            $user->statusidentitas = 1;

            $user->save();

            /* Jasa */
            $jasa = new Jasa;
            $jasa->user_id = $request->customer;
            $jasa->name = $request->jasa;
            $jasa->deskripsi = $request->deskripsi;
            $jasa->provinsi = $request->provinsi;
            $jasa->kabkota = $request->kabkota;
            $jasa->kecamatan = $request->kecamatan;
            $jasa->desa = $request->desa;
            $jasa->alamat = $request->alamat;

            foreach ($request->file('image') as $imagefile) {
                $image = Image::make($imagefile);
                $filename = rand() . '.' . $imagefile->getClientOriginalExtension();
                $image->stream();
                Storage::disk('public_upload')->put('images/jasa/' . $filename, $image, 'public');
                $imagename[] = $filename;
            }
            $jasa->image = implode("|", $imagename);
            $jasa->save();

            $message = [
                'subject' => 'Pendaftaran Mitra oleh Bantuin Hero.',
                'greeting' => 'Hi',
                'body' => 'Ada mitra yang didaftarkan oleh Bantuin Hero. Silahkan klik tombol dibawah untuk menuju halaman review pendaftaran Bantuin Mitra',
                'short' => 'Ada mitra yang didaftarkan oleh Bantuin Hero',
                'thanks' => 'Terima Kasih.',
                'actionText' => 'Buka Halaman',
                'actionURL' => route('admin.hero.mitra.index'),
            ];
            \Notification::send(User::where('level', '3')->get(), new VerifyHeroMitra($message));

            return redirect()->back()->with([
                'status' => 'Penambahan mitra berhasil. Menunggu verifikasi dari admin',
                'class' => 'alert-success'
            ]);
        }
        else {
            return redirect()->back()->with([
                'status' => "Pendaftaran customer gagal: " . implode(", ", $validator->errors()->all()),
                'class' => 'alert-danger'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
