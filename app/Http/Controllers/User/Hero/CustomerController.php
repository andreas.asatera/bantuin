<?php

namespace App\Http\Controllers\User\Hero;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'nohp' => ['required', 'min:9', 'unique:users'],
                'birthday' => ['required'],
                'gender' => ['required'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ],
            [
                'name.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'nohp.required' => 'Kolom nomor hp harus di isi',
                'birthday.required' => 'Kolom tanggal ulang tahun harus di isi',
                'gender.required' => 'Kolom gender harus di isi',
                'password.required' => 'Kolom password harus di isi',
                'email.unique' => 'Email sudah digunakan',
                'nohp.unique' => 'Nomor HP sudah digunakan'
            ]);
        if ($validator->passes()) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'nohp' => $request->nohp,
                'birthday' => date('Y-m-d', strtotime($request->birthday)),
                'gender' => $request->gender,
                'password' => Hash::make($request->password),
                'ref' => Auth::id(),
            ]);
            $user->sendEmailVerificationNotification();
            return redirect()->back()->with([
                'status' => 'Penambahan customer berhasil. Silahkan lakukan verifikasi email customer',
                'class' => 'alert-success'
            ]);
        }
        else{
            return redirect()->back()->with([
                'status' => "Pendaftaran customer gagal: " . implode(", ", $validator->errors()->all()),
                'class' => 'alert-danger'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
