<?php

namespace App\Http\Controllers\User;

use App\Order;
use App\Payment;
use App\Product;
use App\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
        $user = Auth::user();

        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('user-order')->with([
                'user' => $user,
                'orders' => $order->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get()
            ]);
        }

        return view('user.order', [
            'user' => $user,
            'orders' => $order->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function confirm(Request $request)
    {
        //
        $rating = new Rating();
        $rating->order_id = $request->orderid;
        $rating->user_id = Auth::id();
        $rating->status = $request->konfirmasi;
        if ($request->has('komplain')){
            $rating->komplain = $request->komplain;
        }
        if ($request->has('rating')) {
            $rating->rating = $request->rating;

            $payment = Payment::where('order_id', $request->orderid)->first();
            $payment->status = 4;
            $payment->save();
        }
        if ($request->has('review')) {
            $rating->review = $request->review;
        }
        $rating->save();
        return redirect()->back();
    }

    public function ordercancel(Request $request)
    {
        //
        $order = Order::find($request->input('orderid'));
        $order->status = 5;
        $order->save();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
