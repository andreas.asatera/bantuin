<?php

namespace App\Http\Controllers\User;

use App\Jasa;
use App\Notifications\VerifyHero;
use App\Notifications\VerifyJasaAdmin;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('checkidentitas')->only(['create']);
    }

    public function index()
    {
        //
        $user = Auth::user();
        $customers = User::where('ref', $user->id)->get();
        $mitras = Jasa::whereHas('user', function ($q){
            $q->where('ref', "=", Auth::id());
        })->get();

        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();
            return redirect()->route('hero.index')->with([
                'user' => $user,
                'customers' => $customers,
                'mitras' => $mitras
            ]);
        }

        return view('user.hero', [
            'user' => $user,
            'customers' => $customers,
            'mitras' => $mitras
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = Auth::user();
        $user->hero = 1;
        $user->save();
        $message = [
            'subject' => 'Pendaftaran Bantuin Hero.',
            'greeting' => 'Hi',
            'body' => 'Ada customer yang mendaftarkan diri sebagai Bantuin Hero. Silahkan klik tombol dibawah untuk menuju halaman review pendaftaran Bantuin Hero',
            'short' => 'Ada customer yang mendaftar sebagai Bantuin Hero',
            'thanks' => 'Terima Kasih.',
            'actionText' => 'Buka Halaman',
            'actionURL' => route('admin.hero.index'),
        ];
        \Notification::send(User::where('level', '3')->get(), new VerifyHero($message));
        return redirect()->back()->with([
            'status' => 'Pendaftaran sebagai Bantuin Hero sukses. Menunggu konfirmasi dari admin.',
            'class' => 'alert-success'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
