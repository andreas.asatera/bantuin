<?php

namespace App\Http\Controllers\User;

use App\Product;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = new Product;
        $product->jasa_id = $request->jasaid;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $product->detail = $request->detail;
        $validator = Validator::make($request->all(), [
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:4096',
        ]);
        if ($validator->passes()) {
            foreach ($request->file('image') as $imagefile) {
                $image = Image::make($imagefile);
                $image->fit(300, 350, function ($constraint) {
                    $constraint->upsize();
                });
                $filename = rand() . '.' . $imagefile->getClientOriginalExtension();
                $image->stream();
                Storage::disk('public_upload')->put('images/product/' . $filename, $image, 'public');
                $imagename[] = $filename;
            }
            $product->image = implode("|", $imagename);
        }
        $product->save();
        return redirect()->route('user-jasa');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        if (Auth::id() == $product->jasa->user->id) {
            switch ($request->action){
                case 1:
                    $product->name = $request->jasa;
                    $product->price = $request->price;
                    $product->category_id = $request->category;
                    $product->detail = $request->detail;
                    break;
                case 2: // Menonaktifkan
                    $product->status = 2;
                    break;
                case 3: // Mengaktifkan
                    $product->status = 1;
                    break;
                case 4: // Menghapus
                    $product->status = 4;
                    $product->save();
                    return redirect()->back()->with('error', "Layanan <b style='color: black'>$product->name</b> telah dihapus");
                    break;
                default: break;
            }
            $product->save();
            return redirect()->back()->with('status', 'Berhasil merubah layanan');
        } else {
            return redirect()->back()->with('error', 'Terdapat kesalahan pada sistem');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
