<?php

namespace App\Http\Controllers\User;

use App\Mail\VerifyId;
use App\Notifications\VerifyIdAdmin;
use App\User;
use File;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Http\Controllers\Controller;
use Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $provinces = \Indonesia::allProvinces();

        if (isset($_GET['notif_id'])){
            $notifid = $_GET['notif_id'];
            $user->unreadNotifications->where('id', $notifid)->markAsRead();

            return redirect()->route('user')->with([
                'user' => $user,
                'provinces' => $provinces
            ]);
        }

        return view('user.index', [
            'user' => $user,
            'provinces' => $provinces
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'noidentitas' => 'nullable|string|unique:users,noidentitas,' . Auth::id(),
                'fotoidentitas' => 'mimes:jpeg,png,jpg,gif|max:2048',
            ],
            [
                'noidentitas.unique' => 'No identitas sudah digunakan',
                'fotoidentitas.mimes' => 'Upload foto identitas menggunakan format jpeg/jpg/png/gif',
                'fotoidentitas.max' => 'Ukuran maksimal foto identitas yaitu 2048 KB',
            ]);
        $user = Auth::user();
        if ($validator->passes()) {
            if ($request->has('name')) {
                $user->name = $request->name;
            }
            if ($request->has('nohp')) {
                $user->nohp = $request->nohp;
            }
            if ($request->has('gender')) {
                $user->gender = $request->gender;
            }
            if ($request->has('birthday')) {
                $user->birthday = date('Y-m-d', strtotime($request->birthday));
            }
            if ($request->has('noidentitas')) {
                $user->noidentitas = $request->noidentitas;
            }
            if ($request->has('provinsi')) {
                $user->provinsi = $request->provinsi;
            }
            if ($request->has('kabkota')) {
                $user->kabkota = $request->kabkota;
            }
            if ($request->has('kecamatan')) {
                $user->kecamatan = $request->kecamatan;
            }
            if ($request->has('desa')) {
                $user->desa = $request->desa;
            }
            if ($request->has('alamat')) {
                $user->alamat = $request->alamat;
            }
            if ($request->hasFile('fotoidentitas')) {
                File::delete('/images/identitas/' . $user->fotoidentitas);
                $image = Image::make($request->file('fotoidentitas'));
                $filename = rand() . '.' . $request->file('fotoidentitas')->getClientOriginalExtension();
                $image->stream();
                Storage::disk('public_upload')->put('images/identitas/' . $filename, $image, 'public');
                $user->fotoidentitas = $filename;
            }
            if ($request->has('noidentitas') || $request->hasFile('fotoidentitas')) {
                if (isset($user->noidentitas) && isset($user->fotoidentitas)) {
                    \Notification::send(User::where('level', '3')->get(), new VerifyIdAdmin($user));
                    $user->statusidentitas = 1;
                }
            }
            $user->alasan = null;
            $user->save();
            return redirect()->back()->with('status', 'Data berhasil disimpan');
        } else {
            return redirect()->back()->with('errors', $validator->errors()->all());
//            dd($validator->errors()->all());
        }
    }

    public function updatepp(Request $request)
    {
        //
        $user = Auth::user();
        $validator = Validator::make($request->all(),
            [
                'pp' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            ],
            [
                'pp.mimes' => 'Upload foto profil menggunakan format jpeg/jpg/png/gif',
                'pp.max' => 'Ukuran maksimal foto profil yaitu 2048 KB',
            ]);
        if ($validator->passes()) {
            File::delete('/images/pp/'. $user->pp);
            $image = Image::make($request->file('pp'));
            $filename = rand() . '.' . $request->file('pp')->getClientOriginalExtension();
            $image->stream();
            Storage::disk('public_upload')->put('images/pp/' . $filename, $image, 'public');
            $user->pp = $filename;
            $user->save();

            return response()->json([
                'status' => 1,
                'message' => 'Foto profil berhasil diganti.',
                'class' => 'alert alert-success'
            ]);
        }

        return response()->json([
            'status' => 2,
            'message' => $validator->errors()->all(),
            'class' => 'alert alert-danger'
        ]);
    }

    public function updatepass(Request $request)
    {
        //
        if (!(Hash::check($request->get('oldpw'), Auth::user()->password))) {
            // The passwords matches
            return redirect('user/profile#password')->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (strcmp($request->get('oldpw'), $request->get('newpw')) == 0) {
            //Current password and new password are same
            return redirect('user/profile#password')->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        if ($request->get('newpw') !== $request->get('newpw_confirmation')) {
            // New password and confirmation password
            return redirect('user/profile#password')->with("error", "New Password does not matches with Password Confirmation.");
        }
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('newpw'));
        $user->save();
        return redirect('user/profile#password')->with("success", "Your password has been changed.");
    }
}
