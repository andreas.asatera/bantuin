<?php

namespace App\Http\Controllers\Jasa;

use App\Category;
use App\Jasa;
use App\Product;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    //
    public function show(Request $request){
        $product = Product::query()->where([
            ['name', 'LIKE', "%{$request->input('q')}%"],
            ['status', '<>', '4']
        ])->paginate(8);
        $user = Auth::user();
        $jasa = Jasa::all();
        $parent = Category::whereNull('parent_id')->get();
        return view('jasa.index', [
            'user' => $user,
            'jasaList' => $jasa,
            'parents' => $parent,
            'productList' => $product,
            'q' => $request->input('q')
        ]);
    }
}
