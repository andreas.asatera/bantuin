<?php

namespace App\Http\Controllers\Jasa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Jasa;
use App\Category;
use App\Product;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null, $name = null)
    {
//
        $user = Auth::user();
        $jasa = Jasa::all();
        $parent = Category::whereNull('parent_id')->get();
        if (isset($id) && isset($name)) {
            $product = Product::whereHas('jasa')->where([
                ['category_id', $id],
                ['status', '<>', 4]
            ])->paginate(8);
        } else {
            $product = Product::whereHas('jasa')->where('status', '<>', '4')->paginate(8);
        }
        return view('jasa.index', [
            'user' => $user,
            'jasaList' => $jasa,
            'parents' => $parent,
            'productList' => $product
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function show(Jasa $jasa)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function edit(Jasa $jasa)
    {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jasa $jasa)
    {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Jasa $jasa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jasa $jasa)
    {
//
    }

    public function detail($id, $name)
    {
//
        $user = Auth::user();
        $product = Product::find($id);
        if ($product->status != 4){
            return view('jasa.product', [
                'user' => $user,
                'product' => $product
            ]);
        }
        else{
            return redirect()->route('jasa')->with('error', 'Terjadi kesalahan pada sistem. Silahkan ulangi beberapa saat lagi.');
        }
    }
}
