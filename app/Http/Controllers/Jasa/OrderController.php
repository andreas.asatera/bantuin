<?php

namespace App\Http\Controllers\Jasa;

use App\Notifications\OrderNotification;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $user = Auth::user();
        $product = Product::find($id);
        if ($product->status != 4) {
            if ($product->jasa->user->id == Auth::id()) {
                return redirect()->route('jasa-detail', [
                    'id' => $id,
                    'name' => str_replace(' ', '-', $product->name)
                ]);
            }
            return view('jasa.order', [
                'user' => $user,
                'product' => $product
            ]);
        }
        else{
            return redirect()->route('jasa')->with('error', 'Terjadi kesalahan pada sistem. Silahkan ulangi beberapa saat lagi.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $order = new Order;
        $order->products_id = $request->products_id;
        $order->ref = "B".Carbon::now()->format('dmy').str_pad(Auth::id(),3,"000", STR_PAD_LEFT).str_pad(count(Order::whereDate('created_at', Carbon::today())->get()) + 1, 3, "000", STR_PAD_LEFT);
        $order->user_id = Auth::id();
        if ($request->has('note')){
            $order->note = $request->note;
        }
        $order->status = 1;
        $order->save();
        $message = [
            'type' => 1,
            'subject' => 'Pesanan Jasa Baru',
            'greeting' => 'Hi '.$order->product->jasa->user->name,
            'body' => 'Anda mendapat pesanan jasa baru. Silahkan klik dibawah untuk menuju halaman pesanan jasa',
            'short' => 'Anda mendapat pesanan jasa baru',
            'thanks' => 'Terima Kasih.',
            'actionText' => 'Buka Pesanan',
            'actionURL' => route('order'),
        ];
        $order->product->jasa->user->notify(new OrderNotification($message));
        return redirect()->route('user-order');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
