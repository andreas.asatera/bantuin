<?php

namespace App\Http\Controllers;

use App\Product;
use App\Testimoni;
use Illuminate\Http\Request;
use Auth;

class MainController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        $product = Product::withCount(['order' => function ($q) {
            $q->where('status', 4);
        }])->orderBy('order_count', 'desc')->take(10)->get();
        $testimoni = Testimoni::all();
        return view('main.index', [
            'user' => $user,
            'products' => $product,
            'testimonies' => $testimoni
        ]);
    }

    public function about()
    {
        $user = Auth::user();
        return view('main.about', ['user' => $user]);
    }

    public function faq()
    {
        $user = Auth::user();
        return view('main.faq', ['user' => $user]);
    }
}
