<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes(['verify' => true]);

/* landing page */
Route::get('/', 'MainController@index')->name('main');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/about', 'MainController@about')->name('about');
Route::get('/faq', 'MainController@faq')->name('faq');

/*Admin page*/
Route::get('/admin', 'Admin\AdminController@index')->name('admin')->middleware('auth', 'checkadmin');
Route::get('/admin/user', 'Admin\UserController@index')->name('admin-user')->middleware('auth', 'checkadmin');
Route::get('/admin/user/review/{id}', 'Admin\UserController@show')->where('id', '[0-9]+')->name('admin-user-review')->middleware('auth', 'checkadmin');
Route::post('/admin/user/review/', 'Admin\UserController@verification')->name('admin-user-verification')->middleware('auth', 'checkadmin');
Route::get('/admin/jasa', 'Admin\JasaController@index')->name('admin-jasa')->middleware('auth', 'checkadmin');
Route::get('/admin/jasa/review/{id}', 'Admin\JasaController@review')->where('id', '[0-9]+')->name('admin-jasa-review')->middleware('auth', 'checkadmin');
Route::post('/admin/jasa/review/{id}', 'Admin\JasaController@reviewpost')->where('id', '[0-9]+')->name('admin-jasa-review-post')->middleware('auth', 'checkadmin');
Route::get('/admin/category', 'Admin\CategoryController@index')->name('admin-category')->middleware('auth', 'checkadmin');
Route::post('/admin/category', 'Admin\CategoryController@store')->name('admin-create-category')->middleware('auth', 'checkadmin');
Route::get('/admin/category/edit', 'Admin\CategoryController@edit')->name('admin-category-edit')->middleware('auth', 'checkadmin');
Route::post('/admin/category/edit', 'Admin\CategoryController@update')->name('admin-category-update')->middleware('auth', 'checkadmin');
Route::resource('/admin/adminpayment', 'Admin\PaymentController')->middleware(
    'auth', 'checkadmin'
);
Route::resource('/admin/site', 'Admin\SiteController')->middleware('auth', 'checkadmin');
Route::resource('/admin/hero/mitra', 'Admin\Hero\MitraController', ['as' => 'admin.hero'])->middleware('auth', 'checkadmin');
Route::resource('/admin/hero', 'Admin\HeroController', ['as' => 'admin'])->middleware('auth', 'checkadmin');

/* User */
Route::get('/user/profile', 'User\UserController@index')->name('user');
Route::post('/user/profile', 'User\UserController@update')->name('updateuser');
Route::post('/user/updatepp', 'User\UserController@updatepp')->name('updatepp');
Route::post('/user/updatepass', 'User\UserController@updatepass')->name('updatepass');
Route::get('/user/jasa', 'User\JasaController@index')->name('user-jasa')->middleware('checkidentitas');
Route::post('/user/jasa', 'User\JasaController@create')->name('user-create-jasa');
Route::post('/user/jasa/imgupload', 'User\JasaController@imgupload')->name('user-imgupload');
Route::get('/user/order', 'User\OrderController@show')->name('user-order')->middleware('auth');
Route::post('/user/order/konfirmasi', 'User\OrderController@confirm')->name('user-order-confirm')->middleware('auth');
Route::post('/user/order/cancel', 'User\OrderController@ordercancel')->name('user-order-cancel')->middleware('auth');
Route::resource('/user/hero', 'User\HeroController')->middleware('auth', 'verified');
Route::resource('/user/hero/customer', 'User\Hero\CustomerController', ['as' => 'hero'])->middleware('auth', 'verified');
Route::resource('/user/hero/mitra', 'User\Hero\MitraController', ['as' => 'hero'])->middleware('auth', 'verified');

/* Jasa */
Route::get('/jasa', 'Jasa\JasaController@index')->name('jasa');
Route::get('/jasa/c/{id}-{name}', 'Jasa\JasaController@index')->name('jasa-category')->where('id', '[0-9]+');
Route::get('/jasa/s/{id}-{name}', 'Jasa\JasaController@detail')->name('jasa-detail')->where('id', '[0-9]+');
Route::get('/jasa/o/{id?}-{name?}', 'Jasa\OrderController@index')->name('jasa-order')->where('id', '[0-9]+')->middleware('auth', 'checkalamat', 'checkidentitas');
Route::post('/jasa/o', 'Jasa\OrderController@store')->name('jasa-order-add')->where('id', '[0-9]+')->middleware('auth', 'checkalamat');
Route::get('/jasa/search/', 'Jasa\SearchController@show')->name('jasa-search');

/* Order */
Route::get('/order', 'Order\OrderController@index')->name('order')->middleware('checkjasa');
Route::post('/order/store', 'Order\OrderController@store')->name('order-store')->middleware('checkjasa');
Route::post('/order/done', 'Order\OrderController@done')->name('order-done')->middleware('checkjasa');
Route::resource('/order/userpayment', 'PaymentController');

/* Product */
//Route::post('/product/add', 'User\ProductController@store')->name('product-add');
Route::resource('product', 'User\ProductController');

/* Ajax */
Route::get('/ajax/address/provinsi/{id?}', 'Ajax\AddressController@provinsi')->name('ajax-address-provinsi');
Route::get('/ajax/address/kabkota/{id?}', 'Ajax\AddressController@kabkota')->name('ajax-address-kabkota');
Route::get('/ajax/address/kecamatan/{id?}', 'Ajax\AddressController@kecamatan')->name('ajax-address-kecamatan');
Route::post('/ajax/address/add', 'Ajax\AddressController@store')->name('ajax-address-store');
Route::resource('favorite', 'User\FavoriteController');

/*Gallery*/
Route::get('/gallery', 'Gallery\GalleryController@index')->name('gallery');

/*Notif*/
Route::get('pemberitahuan/read', 'Pemberitahuan\PemberitahuanController@read')->name('notifmarkasread')->middleware('auth');
Route::resource('pemberitahuan', 'Pemberitahuan\PemberitahuanController')->middleware('auth');