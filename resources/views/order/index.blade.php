@extends('layouts.main')
@section('title', '| Order')
@section('content')
    <section class="blog_categorie_area"></section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 posts-list">
                <div class="single-post row">
                    <div class="col-lg-12 col-md-12 blog_details">
                        <div class="d-flex justify-content-center">
                            <h2>Pesanan Jasa</h2>
                        </div>
                        <nav>
                            <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show"
                                   id="nav-pending-tab" data-toggle="tab"
                                   href="#nav-pending" role="tab"
                                   aria-controls="nav-pending"
                                   aria-selected="true" style="color: inherit">Belum Konfirmasi</a>
                                <a class="nav-item nav-link"
                                   id="nav-checkpayment-tab" data-toggle="tab"
                                   href="#nav-checkpayment" role="tab"
                                   aria-controls="nav-checkpayment"
                                   aria-selected="false" style="color: inherit">Menunggu Pembayaran</a>
                                <a class="nav-item nav-link"
                                   id="nav-onprogress-tab" data-toggle="tab"
                                   href="#nav-onprogress" role="tab"
                                   aria-controls="nav-onprogress"
                                   aria-selected="false" style="color: inherit">Dalam Pengerjaan</a>
                                <a class="nav-item nav-link"
                                   id="nav-done-tab" data-toggle="tab"
                                   href="#nav-done" role="tab"
                                   aria-controls="nav-done"
                                   aria-selected="false" style="color: inherit">Pengerjaan Selesai</a>
                            </div>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade active show"
                                     id="nav-pending" role="tabpanel"
                                     aria-labelledby="nav-pending-tab">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">No. Pemesanan</th>
                                                <th scope="col">Nama Pemesan</th>
                                                <th scope="col">No HP Pemesan</th>
                                                <th scope="col">Jasa</th>
                                                <th scope="col">Tanggal Pemesanan</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($orders->where('status', 1)->count())
                                                @foreach($orders as $order)
                                                    @if($order->status == 1)
                                                        <tr>
                                                            <td>{{ $order->ref }}</td>
                                                            <td>
                                                                {{ $order->user->name }}
                                                            </td>
                                                            <td>{{ $order->user->nohp }}</td>
                                                            <td>{{ $order->product->name }}</td>
                                                            <td>{{ date('d F Y, H:i:s', strtotime($order->created_at)) }}</td>
                                                            <td>
                                                                <a href="#" style="color: inherit" data-toggle="modal"
                                                                   data-target="#order-{{ $order->status . '-' .$order->ref }}"><i
                                                                            class="fa fa-pencil-square-o"
                                                                            aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <th colspan="6" style="text-align: center">Anda tidak memiliki pesanan
                                                    jasa
                                                </th>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade"
                                     id="nav-checkpayment" role="tabpanel"
                                     aria-labelledby="nav-checkpayment-tab">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">No. Pemesanan</th>
                                                <th scope="col">Nama Pemesan</th>
                                                <th scope="col">No HP Pemesan</th>
                                                <th scope="col">Jasa</th>
                                                <th scope="col">Tanggal Pengerjaan</th>
                                                <th scope="col">Keterangan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($orders->where('status', 2)->count())
                                                @foreach($orders as $order)
                                                    @if($order->status == 2 && ($order->payment->status == '1' || $order->payment->status == '2'))
                                                        <tr>
                                                            <td>{{ $order->ref }}</td>
                                                            <td>
                                                                {{ $order->user->name }}
                                                            </td>
                                                            <td>{{ $order->user->nohp }}</td>
                                                            <td>{{ $order->product->name }}</td>
                                                            <td>{{ date('d F Y, H:i:s', strtotime($order->updated_at)) }}</td>
                                                            {{--<td><a href="#" style="color: inherit" data-toggle="modal"
                                                                   data-target="#order-{{ $order->status . '-' .$order->ref }}"><i
                                                                            class="far fa-check-square"
                                                                            aria-hidden="true"></i></a>
                                                            </td>--}}
                                                            @if($order->payment->status == '1')
                                                                <td>Belum dibayar</td>
                                                            @elseif($order->payment->status == '2')
                                                                <td>Menunggu verifikasi pembayaran dari admin</td>
                                                            @endif
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <th colspan="6" style="text-align: center">Anda tidak memiliki pesanan
                                                    jasa yang belum dibayar
                                                </th>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade"
                                     id="nav-onprogress" role="tabpanel"
                                     aria-labelledby="nav-onprogress-tab">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">No. Pemesanan</th>
                                                <th scope="col">Nama Pemesan</th>
                                                <th scope="col">No HP Pemesan</th>
                                                <th scope="col">Jasa</th>
                                                <th scope="col">Tanggal Pengerjaan</th>
                                                <th scope="col">Keterangan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($orders->where('status', 2)->count())
                                                @foreach($orders as $order)
                                                    @if($order->status == 2 && $order->payment->status == '3')
                                                        <tr>
                                                            <td>{{ $order->ref }}</td>
                                                            <td>
                                                                {{ $order->user->name }}
                                                            </td>
                                                            <td>{{ $order->user->nohp }}</td>
                                                            <td>{{ $order->product->name }}</td>
                                                            <td>{{ date('d F Y, H:i:s', strtotime($order->updated_at)) }}</td>
                                                            <td><a href="#" style="color: inherit" data-toggle="modal"
                                                                   data-target="#order-{{ $order->status . '-' .$order->ref }}"><i
                                                                            class="far fa-check-square"
                                                                            aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <th colspan="6" style="text-align: center">Anda tidak memiliki pesanan
                                                    jasa yang dikerjakan
                                                </th>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade"
                                     id="nav-done" role="tabpanel"
                                     aria-labelledby="nav-done-tab">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">No. Pemesanan</th>
                                                <th scope="col">Nama Pemesan</th>
                                                <th scope="col">No HP Pemesan</th>
                                                <th scope="col">Jasa</th>
                                                <th scope="col">Tanggal Penyelesaian</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($orders->where('status', 4)->count())
                                                @foreach($orders as $order)
                                                    @if($order->status == 4)
                                                        <tr>
                                                            <td>{{ $order->ref }}</td>
                                                            <td>
                                                                {{ $order->user->name }}
                                                                @if(isset($order->note))
                                                                    <br>
                                                                    <small><i>catatan</i>: {{$order->note}}</small>
                                                                @endif
                                                            </td>
                                                            <td>{{ $order->user->nohp }}</td>
                                                            <td>{{ $order->product->name }}</td>
                                                            <td>{{ date('d F Y, H:i:s', strtotime($order->time_finish)) }}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <th colspan="6" style="text-align: center">Anda tidak memiliki pesanan
                                                    jasa yang telah diselesaikan
                                                </th>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($orders->count())
        @foreach($orders as $order)
            <div class="modal fade" id="order-{{ $order->status . '-' .$order->ref }}" tabindex="-1" role="dialog"
                 aria-labelledby="orderLabel-{{$order->ref}}"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    @switch($order->status)
                        @case(1)
                        <form action="{{ route('order-store') }}" method="post">
                            @break
                            @case(2)
                            <form action="{{ route('order-done') }}" method="post">
                                @break
                                @default
                                @endswitch
                                @csrf
                                <label>
                                    <input type="text" name="id" value="{{ $order->id }}" hidden>
                                </label>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="orderLabel-{{$order->ref}}">Konfirmasi Jasa</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-4">Nama Pemesan</div>
                                            <div class="col-md-8">: {{ $order->user->name }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">Alamat Pemesan</div>
                                            <div class="col-md-8">: {{ $order->user->alamat }}
                                                , {{ \Indonesia::findVillage($order->user->desa)->name }}
                                                , {{ \Indonesia::findDistrict($order->user->kecamatan)->name }}
                                                , {{ \Indonesia::findCity($order->user->kabkota)->name }}
                                                , {{ \Indonesia::findProvince($order->user->provinsi)->name }}</div>
                                        </div>
                                        @if(isset($order->note))
                                            <div class="row">
                                                <div class="col-md-4">Catatan Pemesanan</div>
                                                <div class="col-md-8">: {{$order->note}}</div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        @switch($order->status)
                                            @case(1)
                                            <button type="submit" class="genric-btn danger-border" name="tolak"
                                                    value="1">
                                                Tolak
                                            </button>
                                            <button type="submit" class="genric-btn info radius" name="terima"
                                                    value="1">
                                                Terima
                                            </button>
                                            @break
                                            @case(2)
                                            <button type="submit" class="genric-btn info radius" name="selesai"
                                                    value="1">
                                                Selesai
                                            </button>
                                            @break
                                            @default
                                        @endswitch
                                    </div>
                                </div>
                            </form>
                </div>
            </div>
        @endforeach
    @endif
@endsection