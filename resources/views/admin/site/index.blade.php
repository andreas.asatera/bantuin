@extends('layouts.admin')
@section('title', 'Setelan Umum')
@section('content')
    <section class="content-header">
        <h1>Setelan Umum</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Setelan Umum</a></li>
        </ol>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $message }}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Migrate</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <form action="{{ route('site.store') }}" method="POST">
                                @csrf
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <button type="submit" class="btn btn-block btn-primary btn-lg" name="action"
                                                value="migrate">Migrate
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <button type="submit" class="btn btn-block btn-warning btn-lg" name="action"
                                                value="rollback">Rollback
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection