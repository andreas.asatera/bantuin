@extends('layouts.admin')
@section('title', 'Review Jasa')
@section('content')
    <section class="content-header">
        <h1>Review Jasa</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin-jasa') }}"><i class="fa fa-dashboard"></i> Jasa</a></li>
            <li class="active">Review Jasa</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-blue text-center">
                        <h3 class="widget-user-username" style="margin-left: 0">{{ $jasa->name }}</h3>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th width="20%">ID</th>
                                            <td>{{ $jasa->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>Penyedia Jasa</th>
                                            <td>{{ $jasa->user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Pembuatan</th>
                                            <td>{{ date('d F Y', strtotime($jasa->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Deskripsi Jasa</th>
                                            <td>{{ $jasa->deskripsi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td>{{ \Indonesia::findProvince($jasa->provinsi)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kabupaten/Kota</th>
                                            <td>{{ \Indonesia::findCity($jasa->kabkota)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th>
                                            <td>{{ \Indonesia::findDistrict($jasa->kecamatan)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Desa</th>
                                            <td>{{ \Indonesia::findVillage($jasa->desa)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>{{ $jasa->alamat }}</td>
                                        </tr>
                                        <tr>
                                            <th>Foto Jasa</th>
                                            @php
                                                $images = explode("|", $jasa->image);
                                            @endphp
                                            <td>
                                                @foreach ($images as $image)
                                                    <img class="img-fluid"
                                                         src="{{ asset("storage/images/jasa/").'/'.$image}}"
                                                         alt="" width="300" height="auto">
                                                @endforeach
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{!! route('admin-jasa-review-post', ['id'=>$jasa->id]) !!}">
                        <div class="box-footer text-center">
                            @csrf
                            <input type="text" name="id" value="{{ $jasa->id }}" hidden="true">
                            @if ($jasa->verified == 0 || $jasa->verified == 2)
                                <button type="submit" class="btn btn-primary" name="status" value="1">Approve</button>
                                @if($jasa->verified != 2)
                                    {{--<button type="submit" class="btn btn-danger" name="status" value="3">Tolak Jasa
                                    </button>--}}
                                    <button type="button" class="btn btn-danger" name="status" data-toggle="modal"
                                            data-target="#tolak">Tolak Jasa
                                    </button>
                                @endif
                            @elseif($jasa->verified != 3)
                                <button type="submit" class="btn btn-danger" name="status" value="2">Ban</button>
                            @endif
                            <button type="submit" class="btn btn-default" name="status" value="4">Batal</button>
                        </div>
                        <div class="modal" id="tolak">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Modal Heading</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <label for="alasan">Alasan Penolakan</label>
                                        <textarea type="text" name="alasan" id="alasan"
                                                  class="form-control"></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary mr-1"
                                                data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" value="3" name="status" class="btn btn-danger" data-toggle="modal"
                                                data-target="#tolak">Tolak Pengajuan Jasa
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection