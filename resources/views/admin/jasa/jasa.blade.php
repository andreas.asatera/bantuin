@extends('layouts.admin')
@section('title', 'Jasa')
@section('content')
<section class="content-header">
	<h1>Jasa</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Jasa</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				@if (Session::has('message') && Session::has('status'))
				<div class="alert alert-{!! Session::get('status') !!} alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{!! Session::get('message') !!}
				</div>
				@endif
				<div class="box-header with-border">
					<h3 class="box-title">Review Jasa</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="box-body">
								@if ($jasaList->count() > 0)
								<table id="jasaList" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nama Jasa</th>
											<th>Penyedia Jasa</th>
											<th>Tanggal Pembuatan</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($jasaList as $jasa)
										<tr>
											<td>{{ $jasa->id }}</td>
											<td>{{ $jasa->name }}</td>
											<td>{{ $jasa->user->name }}</td>
											<td>{{ date('d F Y', strtotime($jasa->created_at)) }}</td>
											@if ($jasa->verified == 0)
											<td><span class="label label-warning">Belum diverifikasi</span></td>
                                            @elseif ($jasa->verified == 2)
                                                <td><span class="label label-danger">Jasa di Ban oleh admin</span></td>
                                            @elseif ($jasa->verified == 3)
                                                <td><span class="label label-danger">Jasa di tolak oleh admin</span></td>
											@else
											<td><span class="label label-success">Sudah diverifikasi</span></td>
											@endif
											<td><a href="{!! route('admin-jasa-review', ['id'=>$jasa->id]) !!}" style="text-decoration: none; color: inherit;"><i class="fa fa-tasks" aria-hidden="true"></i></a></td>
										</tr>
										@endforeach
									</tbody>
								</table>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('customcss')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" >
@endsection
@section('customscript')
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#jasaList').DataTable();
	} );
</script>
@endsection