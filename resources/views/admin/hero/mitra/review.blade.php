@extends('layouts.admin')
@section('title', 'Review Mitra by Hero')
@section('content')
    <section class="content-header">
        <h1>Bantuin Hero</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.hero.mitra.index') }}"><i class="fa fa-dashboard"></i> Bantuin Mitra by Hero</a></li>
            <li class="active">Review Bantuin Mitra</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-yellow">
                        <div class="widget-user-image">
                            <img class="img-circle"
                                 src="{{ $user->pp ? asset("storage/images/pp/").'/'.$user->pp : asset("fashiop/img/blog/author.png")}}"
                                 alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{ $userView->name }}</h3>
                        <h5 class="widget-user-desc"> Role:
                            @switch($userView->level)
                                @case(1) User @break
                                @case(3) Admin @break
                            @endswitch
                        </h5>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th>No Hp:</th>
                                            <td>{{ $userView->nohp }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email:</th>
                                            <td>{{ $userView->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>Gender:</th>
                                            <td>@switch($userView->gender)
                                                    @case(1) Laki-laki @break
                                                    @case(3) Perempuan @break
                                                @endswitch</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Ulang Tahun:</th>
                                            <td>{{ date('d F Y', strtotime($userView->birthday)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi:</th>
                                            <td>{{ $userView->provinsi? \Indonesia::findProvince($userView->provinsi)->name : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kabupaten/Kota:</th>
                                            <td>{{ $userView->kabkota? \Indonesia::findCity($userView->kabkota)->name : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan:</th>
                                            <td>{{ $userView->kecamatan? \Indonesia::findDistrict($userView->kecamatan)->name : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Desa:</th>
                                            <td>{{ $userView->desa? \Indonesia::findVillage($userView->desa)->name : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat Lengkap:</th>
                                            <td>{{ $userView->alamat? $userView->alamat : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>No. Identitas:</th>
                                            <td>{{ $userView->noidentitas? $userView->noidentitas : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Foto Identitas:</th>
                                            <td>{!! $userView->fotoidentitas? '<img src="'.asset("storage/images/identitas/")."/".$userView->fotoidentitas.'" width="250" height="auto">' : '-' !!}</td>
                                        </tr>
                                        <tr>
                                            <th>Jasa ID</th>
                                            <td>{{ $userView->jasa->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>Penyedia Jasa</th>
                                            <td>{{ $userView->jasa->user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Pembuatan</th>
                                            <td>{{ date('d F Y', strtotime($userView->jasa->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Deskripsi Jasa</th>
                                            <td>{{ $userView->jasa->deskripsi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td>{{ \Indonesia::findProvince($userView->jasa->provinsi)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kabupaten/Kota</th>
                                            <td>{{ \Indonesia::findCity($userView->jasa->kabkota)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th>
                                            <td>{{ \Indonesia::findDistrict($userView->jasa->kecamatan)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Desa</th>
                                            <td>{{ \Indonesia::findVillage($userView->jasa->desa)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>{{ $userView->jasa->alamat }}</td>
                                        </tr>
                                        <tr>
                                            <th>Foto Jasa</th>
                                            @php
                                                $images = explode("|", $userView->jasa->image);
                                            @endphp
                                            <td>
                                                @foreach ($images as $image)
                                                    <img class="img-fluid"
                                                         src="{{ asset("storage/images/jasa/").'/'.$image}}"
                                                         alt="" width="300" height="auto">
                                                @endforeach
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if(isset($userView->statusidentitas) && $userView->statusidentitas != 2)
                            <div class="row">
                                <form action="{{ route('admin.hero.mitra.update', ['id' => $userView->id]) }}" method="POST" >
                                    <div class="col-md-12 text-center">
                                        @csrf
                                        @method('PATCH')
                                        <button type="submit" value="1" name="status" class="btn btn-success">Verifikasi
                                            Mitra
                                        </button>
                                        <button type="button" class="btn btn-danger" style="margin-left: 8px"
                                                data-toggle="modal" data-target="#tolak">Tolak Verifikasi Mitra
                                        </button>
                                    </div>
                                    <div class="modal" id="tolak">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Modal Heading</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <label for="alasan">Alasan Penolakan</label>
                                                    <textarea type="text" name="alasan" id="alasan"
                                                              class="form-control"></textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-secondary mr-1"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    <button type="submit" value="2" name="status" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#tolak">Tolak Verifikasi User
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
        </div>
    </section>
@endsection