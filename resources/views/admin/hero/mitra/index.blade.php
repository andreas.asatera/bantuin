@extends('layouts.admin')
@section('title', 'Hero Mitra')
@section('content')
    <section class="content-header">
        <h1>Hero</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Mitra</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Review Bantuin Mitra by Bantuin Hero</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="usersList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Hero</th>
                                        <th>Status Bantuin Mitra</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($mitras as $mitra)
                                        <tr>
                                            <td>{{$mitra->id}}</td>
                                            <td>{{$mitra->name}}</td>
                                            <td>{{$mitra->email}}</td>
                                            <td>{{\App\User::find($mitra->ref)->name}}</td>
                                            <td>
                                                @php
                                                    switch ($mitra->statusidentitas){
                                                    case 1:
                                                    echo '<span class="label label-warning">Belum diverifikasi</span>';
                                                    break;
                                                    case 2:
                                                    echo '<span class="label label-success">Sudah diverifikasi</span>';
                                                    break;
                                                    }
                                                @endphp
                                            </td>
                                            <td><a href="{!! route('admin.hero.mitra.show', ['id'=>$mitra->id]) !!}"
                                                   style="text-decoration: none; color: inherit;"><span class="glyphicon glyphicon-tasks"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customcss')
    <link rel="stylesheet"
          href="{{ asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('customscript')
    <script src="{{ asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#usersList').DataTable()
        })
    </script>
@endsection