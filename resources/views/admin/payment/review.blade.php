@extends('layouts.admin')
@section('title', 'Review Jasa')
@section('content')
    <section class="content-header">
        <h1>Review Pembayaran</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('adminpayment.index') }}"><i class="fa fa-dashboard"></i> Pembayaran</a></li>
            <li class="active">Review Pembayaran</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-blue text-center">
                        <h3 class="widget-user-username"
                            style="margin-left: 0">{{ $payment->order->product->name }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2">ID</div>
                                    <div class="col-md-10">: {{ $payment->id }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Nama Pengguna</div>
                                    <div class="col-md-10">: {{ $payment->order->user->name }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Nama Layanan</div>
                                    <div class="col-md-10">: <a
                                                href="{{ route('jasa-detail', ['id' => $payment->order->product->id, 'name' => urlencode(str_replace(' ', '-', $payment->order->product->name))]) }}"
                                                target="_blank">{{ $payment->order->product->name }}</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Tanggal Pembayaran</div>
                                    <div class="col-md-10">
                                        : {{ date('d F Y, H:i:s', strtotime($payment->created_at)) }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Nominal Pembayaran</div>
                                    <div class="col-md-10">: Rp. {{ number_format($payment->nominal,0,',','.') }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Bukti Pembayaran</div>
                                    <div class="col-md-10">:<img
                                                src="{{ asset('storage/images/payment/'. $payment->bukti) }}" alt=""
                                                class="img-responsive" width="50%"></div>
                                </div>
                                {{--<div class="table-responsive">
                                    --}}{{--<table class="table">
                                        <tbody>
                                        <tr>
                                            <th width="20%">ID</th>
                                            <td>{{ $jasa->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>Penyedia Jasa</th>
                                            <td>{{ $jasa->user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Pembuatan</th>
                                            <td>{{ date('d F Y', strtotime($jasa->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Deskripsi Jasa</th>
                                            <td>{{ $jasa->deskripsi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td>{{ \Indonesia::findProvince($jasa->provinsi)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kabupaten/Kota</th>
                                            <td>{{ \Indonesia::findCity($jasa->kabkota)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th>
                                            <td>{{ \Indonesia::findDistrict($jasa->kecamatan)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Desa</th>
                                            <td>{{ \Indonesia::findVillage($jasa->desa)->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>{{ $jasa->alamat }}</td>
                                        </tr>
                                        <tr>
                                            <th>Foto Jasa</th>
                                            @php
                                                $images = explode("|", $jasa->image);
                                            @endphp
                                            <td>
                                                @foreach ($images as $image)
                                                    <img class="img-fluid"
                                                         src="{{ asset("storage/images/jasa/").'/'.$image}}"
                                                         alt="" width="300" height="auto">
                                                @endforeach
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>--}}{{--
                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <form method="POST" action="{{ route('adminpayment.update', ['id' => $payment->id]) }}">
                            <div class="box-footer text-center">
                                @csrf
                                @method('PATCH')
                                <input type="text" name="id" value="{{ $payment->id }}" hidden="true">
                                @if($payment->status == 2)
                                    <button type="submit" class="btn btn-primary mr-2" name="status" value="1">Konfirmasi
                                        Pembayaran
                                    </button>
                                @elseif($payment->status == 4)
                                    <button type="submit" class="btn btn-primary" name="status" value="2">Selesaikan
                                        Pembayaran
                                    </button>
                                @endif
                                <button type="submit" class="btn btn-default" name="status" value="3">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection