@extends('layouts.admin')
@section('title', 'Pembayaran')
@section('content')
    <section class="content-header">
        <h1>Pembayaran</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Pembayaran</a></li>
        </ol>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $message }}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pembayaran User</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="paymentsList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama Pembayar</th>
                                        <th>Tanggal Pembayarn</th>
                                        <th>Status Pembayaran</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($payments as $payment)
                                        <tr>
                                            <td>{{$payment->id}}</td>
                                            <td>{{$payment->user->name}}</td>
                                            <td>{{$payment->user->created_at}}</td>
                                            <td>
                                                @php
                                                    switch ($payment->status){
                                                    case 1:
                                                    echo '<span class="label label-info">Pesanan belum dikonfirmasi penyedia jasa</span>';
                                                    break;
                                                    case 2:
                                                    echo '<span class="label label-warning">Belum dikonfirmasi</span>';
                                                    break;
                                                    case 3:
                                                    echo '<span class="label label-success">Sudah dikonfirmasi</span>';
                                                    break;
                                                    case 4:
                                                    echo '<span class="label label-primary">Pesanan telah selesai</span>';
                                                    break;
                                                    case 5:
                                                    echo '<span class="label label-primary">Pembayaran telah dikirim</span>';
                                                    break;
                                                    }
                                                @endphp
                                            </td>
                                            <td><a href="{!! route('adminpayment.show', ['adminpayment'=>$payment->id]) !!}"
                                                   style="text-decoration: none; color: inherit;"><span class="glyphicon glyphicon-tasks"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection