@extends('layouts.admin')
@section('title', 'Kategori')
@section('content')
<section class="content-header">
	<h1>Kategori</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Kategori</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							@if ($message = Session::get('status'))
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								{{ $message }}
							</div>
							@endif
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tambah" data-toggle="tab">Tambah Kategori</a></li>
									@if ($category->count() > 0)
									<li><a href="#tambahsub" data-toggle="tab">Tambah Sub Kategori</a></li>
									@endif
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tambah">
										<form method="POST" action="{{ route('admin-create-category') }}" role="form">
											@csrf
											<div class="box-body">
												<div class="form-group">
													<label for="nama">Nama Kategori</label>
													<input type="text" class="form-control" id="nama" placeholder="Masukkan nama kategori" name="nama">
												</div>
											</div>
											<div class="box-footer">
												<button type="submit" class="btn btn-primary">Tambah Kategori</button>
											</div>
										</form>
									</div>
									@if ($category->count() > 0)
									<div class="tab-pane" id="tambahsub">
										<form method="POST" action="{{ route('admin-create-category') }}" role="form">
											@csrf
											<div class="box-body">
												<div class="form-group">
													<label for="nama">Nama Sub Kategori</label>
													<input type="text" class="form-control" id="nama" placeholder="Masukkan nama sub kategori" name="nama">
												</div>
												<div class="form-group">
													<label>Kategori</label>
													<select class="form-control" name="id">
														@foreach ($category as $cat)
														<option value="{{ $cat->id }}">{{ $cat->id. ". " .$cat->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="box-footer">
												<button type="submit" class="btn btn-primary">Tambah Sub Kategori</button>
											</div>
										</form>
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection