@extends('layouts.admin')
@section('title', 'User')
@section('content')
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Review User</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="usersList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Status Identitas</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($userList as $users)
                                        <tr>
                                            <td>{{$users->id}}</td>
                                            <td>{{$users->name}}</td>
                                            <td>{{$users->email}}</td>
                                            <td>
                                                @php
                                                    switch ($users->statusidentitas){
                                                    case null:
                                                    echo '<span class="label label-info">Belum upload identitas</span>';
                                                    break;
                                                    case 1:
                                                    echo '<span class="label label-warning">Belum diverifikasi</span>';
                                                    break;
                                                    case 2:
                                                    echo '<span class="label label-success">Sudah diverifikasi</span>';
                                                    break;
                                                    }
                                                @endphp
                                            </td>
                                            <td><a href="{!! route('admin-user-review', ['id'=>$users->id]) !!}"
                                                   style="text-decoration: none; color: inherit;"><span class="glyphicon glyphicon-tasks"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customcss')
    <link rel="stylesheet"
          href="{{ asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('customscript')
    <script src="{{ asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#usersList').DataTable()
        })
    </script>
@endsection