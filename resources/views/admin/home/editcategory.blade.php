@extends('layouts.admin')
@section('title', 'Edit Kategori')
@section('content')
<section class="content-header">
	<h1>Edit Kategori</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('admin-category') }}"><i class="fa fa-dashboard"></i> Kategori</a></li>
		<li class="active"> Edit Kategori</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							@if ($message = Session::get('status'))
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								{{ $message }}
							</div>
							@endif
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tr>
										<th style="width: 10px">ID</th>
										<th>Nama</th>
										<th>Jenis</th>
										<th>Aksi</th>
									</tr>
									@foreach ($categories as $category)
									<tr>
										<td style="width: 10px">{{ $category->id }}</td>
										<td>{{ $category->name }}</td>
										@if (is_null($category->parent))
										<td>Kategori</td>
										@else
										<td>Sub Kategori</td>
										@endif
										<td><button type="button" class="btn bg-maroon" data-toggle="modal" data-target="#edit-{{ $category->id }}">Edit</button></td>
									</tr>
									<div class="modal fade" id="edit-{{ $category->id }}">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title">Edit Kategori: {{ $category->name }}</h4>
												</div>
												<form method="POST" action="{{ route('admin-category-update') }}">
													@csrf
													<div class="modal-body">
														<div class="form-group">
															<label for="name">Nama Kategori</label>
															<input type="text" class="form-control" id="name" placeholder="Masukkan nama kategori" required="true" value="{{ $category->name }}" name="name">
														</div>
														<div class="form-group">
															<label>Pilih Kategori</label>
															<select class="form-control" name="jenis">
																<option value="0">Kategori Utama</option>
																<optgroup label="Sub Kategori">
																	@foreach ($parentList as $parent)
																	@if ($category->parent_id == $parent->id)
																	<option value="{{ $parent->id }}" selected="true">{{ $parent->name }}</option>
																	@else
																	<option value="{{ $parent->id }}">{{ $parent->name }}</option>
																	@endif
																	@endforeach
																</optgroup>
															</select>
														</div>
														<input type="text" name="cid" value="{{ $category->id }}" hidden="true">
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
														<button type="submit" class="btn btn-primary">Save changes</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection