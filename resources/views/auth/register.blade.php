@extends('layouts.main')

@section('content')
    <section class="login_box_area p_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <img class="img-fluid" src="{{ asset('fashiop/img/login.jpg')}}" alt="">
                        <div class="hover">
                            <h4>Sudah punya akun?</h4>
                            <a class="main_btn" href="login" style="color: white; background: #F37320;">Masuk</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner reg_form">
                        <h3>Buat Akun</h3>
                        <form class="row login_form" action="{{ route('register') }}" method="post" id="contactForm">
                            @csrf
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                                       required autofocus placeholder="Nama Lengkap">
                                @if ($errors->has('name'))
                                    <strong>{{ $errors->first('name') }}</strong>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="email" class="form-control" id="email" name="email"
                                       value="{{ old('email') }}" required placeholder="Alamat Email">
                                @if ($errors->has('email'))
                                    <strong>{{ $errors->first('email') }}</strong>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="tel" class="form-control" id="nohp" name="nohp"
                                       value="{{ old('nohp') }}" required placeholder="Nomor HP">
                                @if ($errors->has('nohp'))
                                    <strong>{{ $errors->first('nohp') }}</strong>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <select name="gender" class="nice-select wide">
                                    <option value="1" selected>Laki-laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                                @if ($errors->has('gender'))
                                    <strong>{{ $errors->first('gender') }}</strong>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="datepicker" autocomplete="off"
                                           name="birthday" data-date-format="dd-mm-yyyy" placeholder="Tanggal Lahir">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                    </div>
                                </div>
                                @if ($errors->has('birthday'))
                                    <strong>{{ $errors->first('birthday') }}</strong>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password" name="password" required
                                       placeholder="Kata Sandi" autocomplete="new-password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="pass" name="password_confirmation"
                                       required placeholder="Ulang Kata Sandi">
                            </div>
                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="btn submit_btn">Buat Akun</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function () {
            var date_input = $('input[name="birthday"]'); //our date input has the name "birthday"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'dd-mm-yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });
    </script>
@endsection