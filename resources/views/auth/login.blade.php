@extends('layouts.main')

@section('content')
<section class="login_box_area p_120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="login_box_img">
                    <img class="img-fluid" src="{{ asset('fashiop/img/login.jpg')}}" alt="">
                    <div class="hover">
                        <h4>Baru di BANTUIN?</h4>
                        <p>"Anda bantu kami, kami bantu Anda"<br>
                            Dengan Bantuin, semua mudah meminta Bantuan</p>
                        <a class="main_btn" href="register" style="color: white; background: #F37320;">Buat akun baru</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="login_form_inner">
                    <h3>Masuk</h3>
                    <form class="row login_form" action="{{ route('login') }}" method="post" id="contactForm" novalidate="">
                        @csrf
                        <div class="col-md-12 form-group">
                            <input type="email" class="form-control" id="name" name="email" placeholder="Email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <strong>{{ $errors->first('email') }}</strong>
                            @endif
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi" required>
                            @if ($errors->has('password'))
                            <strong>{{ $errors->first('password') }}</strong>
                            @endif
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="creat_account">
                                <input type="checkbox" id="f-option2" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="f-option2">Ingat saya</label>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <button type="submit" value="submit" class="btn submit_btn" id="btnSubmit">Masuk</button>
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                {{ __('Lupa Kata Sandi?') }}
                            </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
