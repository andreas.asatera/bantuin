@extends('layouts.main')
@section('title', '| Jasa')
@section('content')
    <section class="cat_product_area section_gap">
        <div class="container-fluid">
            <div class="row flex-row-reverse">
                <div class="col-lg-9">
                    <div class="latest_product_inner row">
                        @if($productList->count())
                            @if(isset($q))
                                <div class="col-lg-12">
                                    <div class="alert alert-info text-center" role="alert">
                                        <h5>Cari Jasa: "{{ $q }}"</h5>
                                    </div>
                                </div>
                            @endif
                            @foreach ($productList as $product)
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="f_p_item">
                                        <div class="f_p_img">
                                            @php
                                                $images = explode("|", $product->image);
                                            @endphp
                                            <img class="img-fluid"
                                                 src="{{ asset("storage/images/product/").'/'.$images[0]}}" alt="">
                                            <div class="p_icon">
                                                @auth
                                                    <a href="#">
                                                        <i class="{{ in_array($product->id, $user->favorite->pluck('products_id')->toArray()) ? 'fas fa-heart' : 'far fa-heart' }}" aria-hidden="true" data-id="{{ $product->id }}"></i>
                                                    </a>
                                                @endauth
                                                <a href="{{ route('jasa-detail', ['id' => $product->id, 'name' => urlencode(str_replace(' ', '-', $product->name))]) }}">
                                                    <i class="lnr lnr-cart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="{{ route('jasa-detail', ['id' => $product->id, 'name' => urlencode(str_replace(' ', '-', $product->name))]) }}">
                                            <h4>{{ $product->name }}</h4>
                                        </a>
                                        <ul class="list">
                                            <li>
                                                @php
                                                    $rate = round($product->rating->avg('rating'));
                                                    for ($x = 0; $x < $rate; $x++) {
                                                        echo '<i class="fas fa-star" style="color: #f47321"></i>';
                                                    }
                                                    for ($x = 0; $x < 5-$rate; $x++) {
                                                        echo '<i class="far fa-star"></i>';
                                                    }
                                                @endphp
                                                ({{ round($product->rating->avg('rating')) }})
                                            </li>
                                        </ul>
                                        <h5>Rp. {{ number_format($product->price,0,',','.') }}</h5>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            @if(count(\App\Jasa::all()) > 0)
                            <div class="col-lg-12">
                                <div class="alert alert-danger text-center" role="alert">
                                    <h5>Jasa tidak ditemukan</h5>
                                </div>
                            </div>
                                @else
                                <div class="col-lg-12">
                                    <div class="alert alert-danger text-center" role="alert">
                                        <h5>Belum ada jasa yang didaftarkan</h5>
                                    </div>
                                </div>
                                @endif
                        @endif
                        @if(session('error'))
                            <div class="col-lg-12">
                                <div class="alert alert-danger text-center" role="alert">
                                    <h5>{{ session('error') }}</h5>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets cat_widgets">
                            <div class="l_w_title">
                                <h3>Browse Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    @foreach ($parents as $parent)
                                        <li>
                                            <a href="#{{ $parent->id }}" class="caret">{{ $parent->name }} <i
                                                        class="fa fa-plus" style="float: right"></i></a>
                                            @if ($parent->children->count())
                                                <ul class="list">
                                                    @foreach($parent->children as $children)
                                                        <li>
                                                            <a href="{{ route('jasa-category', ['id' => $children->id, 'name' => str_replace(" ", "-", $children->name)]) }} ">{{ $children->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </aside>{{--
                        <aside class="left_widgets p_filter_widgets">
                            <div class="widgets_inner">
                                <h4>Price</h4>
                                <div class="range_item">
                                    <div id="slider-range"></div>
                                    <div class="row m0">
                                        <label for="amount">Price : </label>
                                        <input type="text" id="amount" readonly>
                                    </div>
                                </div>
                            </div>
                        </aside>--}}
                    </div>
                </div>
            </div>
            <div class="row">
                <nav class="cat_page mx-auto" aria-label="Page navigation example">
                    {{ $productList->links() }}
                </nav>
            </div>
        </div>
    </section>
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".p_icon").click(function () {
                var iconheart = $(this).find(".fa-heart");
                $.ajax({
                    url: '{{ route('favorite.store') }}',
                    method: "POST",
                    data: { id: iconheart.data("id") },
                    dataType: 'JSON',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status === 1) {
                            iconheart.toggleClass('fas fa-heart');
                            iconheart.toggleClass('far fa-heart');
                        }
                    }
                });

            })
        });
    </script>
@endsection