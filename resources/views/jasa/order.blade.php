@extends('layouts.main')
@section('title', '| Order Jasa')
@section('content')
    <section class="checkout_area section_gap">
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="order_box">
                        <h2>Pesan Jasa</h2>
                        <label for="note">Masukkan catatan pesanan untuk penyedia jasa (opsional)</label>
                        <form action="{{route('jasa-order-add')}}" method="POST">
                        <textarea name="note" class="form-control" id="note" placeholder="Masukkan catatan pesanan"></textarea>
                        <ul class="list">
                            <li>
                                <a>Nama Jasa
                                    <span>Biaya Jasa</span>
                                </a>
                            </li>
                            <li>
                                <a>{{ $product->name  }}
                                    <span class="last">Rp. {{ number_format($product->price, 0, ',', '.') }}</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="list list_2">
                            <li>
                                <a>Total
                                    <span>Rp. {{ number_format($product->price, 0, ',', '.') }}</span>
                                </a>
                            </li>
                        </ul>
                            @csrf
                            <label>
                                <input type="text" value="{{ $product->id }}" name="products_id" hidden>
                            </label>
                            <div class="d-flex justify-content-center">
                                <button type="submit" class="main_btn" href="#">Pesan Jasa</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection