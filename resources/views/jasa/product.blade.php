@extends('layouts.main')
@section('title', '| ' . $product->name)
@section('content')
    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-4">
                    <div class="s_product_img">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" style="min-height: 0px">
                                @php
                                    $images = explode("|", $product->image);
                                $tab2 = true;
                                $i2 = 0;
                                @endphp
                                @foreach ($images as $image)
                                    <div class="carousel-item {!! $tab2 ? "active" : '' !!}" style="min-height: 0px">
                                        <img class="d-block w-100"
                                             src="{{ asset("storage/images/product/").'/'.$image}}"
                                             onclick="openModal();currentSlide({{ $i2 + 1 }})" alt="{{ $i2 + 1 }}">
                                    </div>
                                    @php
                                        $tab2 = false;
                                        $i2++;
                                    @endphp
                                @endforeach
                            </div>
                            <ol class="carousel-indicators">
                                @php
                                    $tab = true;
                                    $i = 0;
                                @endphp
                                @foreach ($images as $image)
                                    <li data-target="#carouselExampleIndicators"
                                        data-slide-to="{{ $i }}" {!! $tab ? "class='active'" : '' !!}>
                                        <img src="{{ asset("storage/images/product/").'/'.$image}}" alt="" width="60"
                                             height="60">
                                    </li>
                                    @php
                                        $tab = false;
                                        $i++;
                                    @endphp
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    <div class="s_product_text" style="margin-top: 0">
                        <h3>{{ ucwords($product->name) }}</h3>
                        <h2>Rp. {{ number_format($product->price,0,',','.') }}</h2>
                        <ul class="list">
                            <li>
                                @php
                                    $rate = round($product->rating->avg('rating'));
                                    for ($x = 0; $x < $rate; $x++) {
                                        echo '<i class="fas fa-star" style="color: #f47321"></i>';
                                    }
                                    for ($x = 0; $x < 5-$rate; $x++) {
                                        echo '<i class="far fa-star"></i>';
                                    }
                                @endphp
                                ({{ round($product->rating->avg('rating')) }})
                            </li>
                            <li>
                                <a class="active"
                                   href="{{ route('jasa-category', ['id' => $product->category->id, 'name' => str_replace(' ', '-', $product->category->name)]) }}"><span>Kategori</span>: {{ $product->category->name }}
                                </a>
                            </li>
                            <li>
                                <a><span>Status</span>:
                                    @if($product->status == 1)
                                        Tersedia
                                    @else
                                        Tidak Tersedia
                                    @endif
                                </a>
                            </li>
                            {{--<li>
                                <a><span>Alamat</span>:  {{ $product->jasa->user->alamat }}
                                    , {{ \Indonesia::findVillage($product->jasa->user->desa)->name }}
                                    , {{ \Indonesia::findDistrict($product->jasa->user->kecamatan)->name }}
                                    , {{ \Indonesia::findCity($product->jasa->user->kabkota)->name }}
                                    , {{ \Indonesia::findProvince($product->jasa->user->provinsi)->name }}
                                </a>
                            </li>--}}
                        </ul>
                        @if ($product->jasa->user->id != Auth::id())
                            @if($product->status == 1)
                                <div class="card_area">
                                    <a class="main_btn"
                                       href="{{ route('jasa-order', ['id' => $product->id, 'name' => str_replace(' ', '-', $product->name)]) }}">Pesan
                                        Jasa</a>
                                    @auth
                                        <a class="icon_btn" href="#">
                                            <i class="{{ in_array($product->id, $user->favorite->pluck('products_id')->toArray()) ? 'fas fa-heart' : 'far fa-heart' }}"
                                               aria-hidden="true" data-id="{{ $product->id }}"></i>
                                        </a>
                                    @endauth
                                </div>
                            @elseif($product->status == 2)
                                <div class="card_area">
                                    <a class="main_btn disabled"
                                       href="javascript:void(0)">Jasa Tidak Tersedia</a>
                                </div>
                            @endif
                        @endif
                        <section class="product_description_area" style="margin-top: 0">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab"
                                       aria-controls="review" aria-selected="false">Reviews</a>
                                </li>
                            </ul>
                            <div class="tab-content p-0" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <p>{{ $product->detail }}</p>
                                </div>
                                <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="review_list">
                                                @foreach($product->rating as $rating)
                                                    <div class="review_item">
                                                        <div class="media">
                                                            <div class="d-flex">
                                                                <img src="{{ $rating->user->pp ? asset("storage/images/pp/").'/'.$rating->user->pp : asset("fashiop/img/blog/author.png")}}"
                                                                     alt="" width="70px" height="71px">
                                                            </div>
                                                            <div class="media-body">
                                                                <h4>{{ $rating->user->name }}</h4>
                                                                @php
                                                                    $rate2 = (int)$rating->rating;
                                                                    for ($y = 0; $y < $rate2; $y++) {
                                                                        echo '<i class="fa fa-star" style="color: #f47321"></i>';
                                                                    }
                                                                @endphp
                                                            </div>
                                                        </div>
                                                        <p>{{ $rating->review }}</p>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->
    <!--================Product Description Area =================-->
    <div id="myModal" class="modal" style="opacity: 0.95 !important;">
        <span class="close2 cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content-2">
            @php
                $i = 0;
            @endphp
            @foreach ($images as $image)
                <div class="mySlides">
                    <div class="numbertext">{{ $i + 1 }} / {{ count($images) }}</div>
                    <img src="{{ asset("storage/images/product/").'/'.$image}}"
                         style="display:block; margin-left: auto; margin-right: auto;" height="400" width="auto" style="opacity: 1;">
                </div>
                @php
                    $i++;
                @endphp
            @endforeach

        <!-- Next/previous controls -->
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption"></p>
            </div>

            <!-- Thumbnail image controls -->
            @php
                $i = 0;
            @endphp
            @foreach ($images as $image)
                <div class="column">
                    <img class="demo" src="{{ asset("storage/images/product/").'/'.$image}}"
                         onclick="currentSlide({{ $i + 1 }})" alt="" style="width:100px; height: auto; opacity: 1;">
                </div>
                @php
                    $i++;
                @endphp
            @endforeach
        </div>
    </div>
    <!--================End Product Description Area =================-->
@endsection
@section('customcss')
    <style>

        .row > .column {
            padding: 0 8px;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Create four equal columns that floats next to eachother */
        .column {
            float: left;
            width: auto;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
        }

        /* Modal Content */
        .modal-content-2 {
            position: relative;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
        }

        /* The Close Button */
        .close2 {
            color: white;
            position: absolute;
            top: 10px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }

        .close2:hover,
        .close2:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }

        /* Hide the slides by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Caption text */
        .caption-container {
            text-align: center;
            background-color: black;
            padding: 2px 16px;
            color: white;
        }

        img.demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }
    </style>
@endsection
@section('customjs')
    <script type="text/javascript">
            // Open the Modal
            function openModal() {
                document.getElementById('myModal').style.display = "block";
            }

            // Close the Modal
            function closeModal() {
                document.getElementById('myModal').style.display = "none";
            }

            var slideIndex = 1;
            showSlides(slideIndex);

            // Next/previous controls
            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            // Thumbnail image controls
            function currentSlide(n) {
                showSlides(slideIndex = n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("demo");
                var captionText = document.getElementById("caption");
                if (n > slides.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = slides.length
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
                captionText.innerHTML = dots[slideIndex - 1].alt;
            }
            $(".icon_btn").click(function () {
                var iconheart = $(this).find(".fa-heart");
                $.ajax({
                    url: '{{ route('favorite.store') }}',
                    method: "POST",
                    data: { id: iconheart.data("id") },
                    dataType: 'JSON',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status === 1) {
                            iconheart.toggleClass('fas fa-heart');
                            iconheart.toggleClass('far fa-heart');
                        }
                    }
                });
        });
    </script>
@endsection