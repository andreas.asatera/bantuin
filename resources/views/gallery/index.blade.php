@extends('layouts.main')
@section('title', '| Galeri')
@section('content')
    <!--================Blog Categorie Area =================-->
    <section class="blog_categorie_area">
    </section>
    <!--================Blog Categorie Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area">
        <div class="container">
            <div class="text-center">
                <img src="{{ asset('storage/images/banner/comingsoon2-01.jpeg') }}" class="img-responsive" alt="Coming Soon" width="50%" height="auto">
            </div>
            {{--<div class="row justify-content-center">
                <h1>Coming Soon</h1>
            </div>--}}
        </div>
    </section>
    <!--================Blog Area =================-->
@endsection