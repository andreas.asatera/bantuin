@extends('layouts.main')
@section('content')
    <section class="home_banner_area">
        <div class="overlay"></div>
        <div id="carouselExampleControls" class="carousel slide" data-ride="true">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                {{--                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>--}}
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100"
                         src="{{ asset('storage/images/banner/bantuin1.jpg') }}"
                         alt="First slide" height="500">
                    <div class="carousel-caption d-md-block" style="bottom: 35%">
                        <h1>Selamat datang di Bantuin</h1>
                        <h5>Tempat dimana kamu bisa menemukan semua jasa yang kamu cari dengan mudah, cepat dan terpercaya.</h5>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100"
                         src="{{ asset('storage/images/banner/carijasa1.jpg') }}"
                         alt="Second slide" height="500">
                    <div class="carousel-caption d-md-block" style="bottom: 35%">
                        <h1>Cara cepat cari jasa terpercaya</h1>
                        <h5>Mudahnya cari jasa dengan cepat hanya dalam satu genggaman</h5>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100"
                         src="{{ asset('storage/images/banner/daftarjasa.jpg') }}"
                         alt="Third slide" height="500">
                    <div class="carousel-caption d-md-block" style="bottom: 35%">
                        <h1>Lengkapi info jasamu, ambil foto, upload!</h1>
                        <h5>Pasang jasamu di Bantuin, kembangkan usahamu, tingkatkan penghasilanmu</h5>
                    </div>
                </div>
                {{--<div class="carousel-item">
                    <img class="d-block w-100"
                         src="{{ asset('fashiop/img/product/hot_deals/deal1.jpg') }}"
                         alt="Fourth slide" height="500">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Jadilah Bantuin Hero!</h5>
                        <p>Tambah penghasilanmu dengan membantu masyarakat di sekitarmu</p>
                    </div>
                </div>--}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
               data-slide="prev">
                <div class="circle">
                    {{--                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
                    <i class="fa fa-chevron-left" style="font-size:30px;color:#f6f6f7"></i>
                    <span class="sr-only">Previous</span>
                </div>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
               data-slide="next">
                <div class="circle">
                    {{--                    <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
                    <i class="fa fa-chevron-right" style="font-size:30px;color:#f6f6f7"></i>
                    <span class="sr-only">Next</span>
                </div>
            </a>
        </div>
    </section>
    {{--<section class="hot_deals_area section_gap">
        <div class="main_box">
            <div class="container-fluid">
                @if($products->count())
                    <div class="row">
                        <div class="main_title">
                            <h2>Jasa of the Week</h2>
                        </div>
                    </div>
                    <div class="row">
                        @php
                            $i = 1;
                        @endphp
                        @foreach($products as $product)
                            @php
                                $images = explode("|", $product->image);
                            @endphp
                            <div class="col col{{$i}}">
                                <div class="f_p_item">
                                    <div class="f_p_img">
                                        <img class="img-fluid"
                                             src="{{ asset("storage/images/product/").'/'.$images[0]}}" alt="">
                                        <div class="p_icon">
                                            <a href="#">
                                                <i class="lnr lnr-heart"></i>
                                            </a>
                                            <a href="{{ route('jasa-detail', ['id' => $product->id, 'name' => urlencode(str_replace(' ', '-', $product->name))]) }}">
                                                <i class="lnr lnr-cart"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <a href="{{ route('jasa-detail', ['id' => $product->id, 'name' => urlencode(str_replace(' ', '-', $product->name))]) }}">
                                        <h4>{{ $product->name }}</h4>
                                    </a>
                                    <h5>Rp. {{ number_format($product->price,0,',','.') }}</h5>
                                </div>
                            </div>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="main_title">
                            <h2>Bergabunglah dengan Bantuin</h2>
                        </div>
                    </div>
                    <div class="row">
                        <h3 class="text-center">
                            Sebuah aplikasi berisi katalog jasa yang dapat dipilih dengan mudah oleh PENCARI JASA dan
                            dapat terhubung secara langsung dengan PENYEDIA JASA.
                        </h3>
                    </div>
                @endif
            </div>
        </div>
    </section>--}}
    <section class="clients_logo_area section_gap">
        <div class="container-fluid">
            <div class="row">
                <div class="main_title">
                    <h2>MITRA BANTUIN</h2>
                </div>
            </div>
            <div class="clients_slider owl-carousel">
                <div class="item">
                    <a href="https://beadgrup.com/" target="_blank"><img
                                src="{{ asset('storage/images/logo/beadgrup.png') }}"
                                style="width: 195px; height: auto;" alt="Beadgrup"></a>
                </div>
                <div class="item">
                    <a href="https://www.tomboltech.com/" target="_blank"><img
                                src="{{ asset('storage/images/logo/tomboltech.png') }}"
                                style="width: 195px; height: auto;" alt="Tomboltech"></a>
                </div>
                <div class="item">
                    <a href="https://umrohbnwkediri.co.id/" target="_blank"><img
                                src="{{ asset('storage/images/logo/umroh.jpeg') }}" style="width: 195px; height: auto;"
                                alt="Umroh & Haji"></a>
                </div>
            </div>
        </div>
    </section>
    @if($testimonies->count() > 0)
        <section class="hot_deals_area section_gap">
            <div class="main_box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="main_title">
                            <h2>Testimoni Bantuin</h2>
                        </div>
                    </div>
                    <div class="main-gallery">
                        <div class="gallery-cell">
                            <div class="testimonial">
                                <img class="testimonial-avatar"
                                     src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg" alt="">
                                <q class="testimonial-quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Duis mauris
                                    ex, gravida ut leo eu, rhoncus porta orci. Fusce vitae rutrum nulla."</q>
                                <span class="testimonial-author">Joe Smith, CEO of Cubix</span>
                            </div>
                        </div>
                        <div class="gallery-cell">
                            <div class="testimonial">
                                <img class="testimonial-avatar"
                                     src="https://s3.amazonaws.com/uifaces/faces/twitter/chexee/128.jpg" alt="">
                                <q class="testimonial-quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Duis mauris
                                    ex, gravida ut leo eu, rhoncus porta orci. Fusce vitae rutrum nulla."</q>
                                <span class="testimonial-author">Lisa Jones, Freelance Web Developer</span>
                            </div>
                        </div>
                        <div class="gallery-cell">
                            <div class="testimonial">
                                <img class="testimonial-avatar"
                                     src="https://s3.amazonaws.com/uifaces/faces/twitter/andretacuyan/128.jpg" alt="">
                                <q class="testimonial-quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Duis mauris
                                    ex, gravida ut leo eu, rhoncus porta orci. Fusce vitae rutrum nulla."</q>
                                <span class="testimonial-author">Ryan Waltz, Front-End Developer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
@section('customcss')
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <style type="text/css">
        .main-gallery {
            background: #fff;
        }

        .gallery-cell {
            width: 100%;
        }

        .testimonial {
            text-align: center;
            max-width: 850px;
            margin: 100px auto 130px auto;
            padding: 0 20px;
        }

        .testimonial-avatar {
            width: 100px;
            border-radius: 50%;
        }

        .testimonial-quote {
            display: block;
            font-size: 24px;
            font-weight: 300;
            padding: 10px 0;
        }

        .testimonial-author {
            display: block;
            font-weight: 800;
            color: #7AA641;
        }

        .flickity-page-dots {
            bottom: 25px;
        }

        .flickity-page-dots .dot.is-selected {
            background: #7AA641;
        }

        .circle {
            background: #777777;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            justify-content: center;
            align-items: center;
            text-align: center;
            display: flex;
        }

        /*
                a.carousel-control-prev:hover {
                    background: black;
                    opacity: 0.3;
                }*/

        /*a.carousel-control-next:hover {
            background: black;
            opacity: 0.3;
        }*/

        .carousel-control-next, .carousel-control-prev {
            width: 10%;
        }
    </style>
@endsection
@section('customjs')
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script type="text/javascript">
        var flkty = new Flickity('.main-gallery', {
            cellAlign: 'left',
            contain: true,
            wrapAround: true,
            prevNextButtons: true,
            pauseAutoPlayOnHover: false,
            autoplay: true,
            autoPlay: 5000
        });
    </script>
@endsection