@extends('layouts.main')
@section('title', '| FAQ')
@section('content')
    <!--================Blog Categorie Area =================-->
    <section class="blog_categorie_area">
    </section>
    <!--================Blog Categorie Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area">
        <div class="container">
            <div class="main_title">
                <h2>BANTUAN</h2>
                <h4>Pertanyaan-pertanyaan yang sering ditanyakan</h4>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apakah Bantuin itu?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Aplikasi Berisi Katalog JASA yang dapat dipilih dengan mudah oleh PENCARI JASA dan dapat terhubung secara langsung dengan PENYEDIA JASA.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apa tujuan dari Bantuin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Penyetaraan Harga, Mensejahterakan Masyarakat, Memudahkan Pencarian Jasa, Memudahkan Pencarian Jasa.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara memesan jasa menggunakan Bantuin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Pertama cari jasa yang anda butuhkan dengan memanfaatkan fitur kategori jasa atau kolom pencarian. Setelah menemukan jasa yang anda inginkan maka klik tombol pesan jasa tersebut.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    <h3>
                        <i></i>
                        <span class="title-name">Apakah menggunakan jasa di Bantuin itu aman?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Aman. Karena kami memiliki semua data penyedia jasa secara detail dan lengkap demi keamanan. Serta semua penyedia jasa telah melalui tahap verifikasi sebelum di unggah.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    <h3>
                        <i></i>
                        <span class="title-name">Fitur apa saja yang dimiliki Bantuin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Kategori, Maps, Rating & Review, Notifikasi.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara mendaftarkan jasa saya di aplikasi Bantuin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Buat Akun dan lengkapi profil, Tambahkan Foto Hasil Jasa, Upload Jasa Bantuin.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apa keuntungan memakai aplikasi Bantuin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Dengan aplikasi Bantuin akan mempermudah mencari jasa terdekat dan akurat yang ada disekitar anda serta kapanpun anda membutuhkanya.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana metode pembayaran Bantuin Jasa?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Ada 2 metode yang dapat digunakan untuk proses pembayaran. Yaitu dengan transfer ATM serta pembayaran melalui sistem COD.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apa kelebihan aplikasi Bantuin dengan aplikasi sejenis?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Selain kemudahan mencari jasa, aplikasi Bantuin memilki kelebihan antara lain penyedia jasa yang terverifikasi, memiliki review pengerjaan jasa, jasa yang ditampilkan selalu up to date, serta memiliki harga yang transparan.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apa itu Bantuin Hero?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Bantuin Hero merupakan orang yang membantu para penyedia jasa memasarkan/ memposting jasanya di aplikasi bantuin. Melalui Bantuin Hero, penyedia jasa dapat lebih dikenal oleh masyarakat.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Apa tugas dari Bantuin Hero?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Tugas dari Bantuin Hero adalah mencari jasa yang ada di sekitar, serta mencari para penyedia jasa yang mulai ditinggalkan untuk membantu mereka agar bisa dikenal oleh masyarakat sehingga masyarakat tidak kesusahan saat mencari mereka.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara mendaftar menjadi Bantuin Hero?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Silahkan mendaftar melalui website atau aplikasi Android dan memasukan data diri anda secara lengkap untuk kemudian diverifikasi oleh pihak admin apakah anda layak menjadi Bantuin Hero.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara mendapatkan Koin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Anda dapat membeli koin pada aplikasi Bantuin denga cara transfer dana melalui ATM ke rekening Bantuin sesuai nominal yang tertera.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara menggunakan Koin?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Koin dapat digunakan untuk mencari tahu secara detail dari penyedia jasa yang anda butuhkan.</p>
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title ">
                    <h3>
                        <i></i>
                        <span class="title-name">Bagaimana cara menghubungi kami?</span>
                    </h3>
                </div>
                <div class="toggle-inner">
                    <p>Melalui Instgram atau Contact Service kami yang tertera di Website.</p>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function () {
            if (jQuery(".toggle .toggle-title").hasClass('active')) {
                jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
            }
            jQuery(".toggle .toggle-title").click(function () {
                if (jQuery(this).hasClass('active')) {
                    jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
                } else {
                    jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
                }
            });
        });
    </script>
@endsection
@section('customcss')
    <style>
        /* Styles for Accordion */
        .toggle:last-child {
            border-bottom: 1px solid #dddddd;
        }

        .toggle .toggle-title {
            position: relative;
            display: block;
            border-top: 1px solid #dddddd;
            margin-bottom: 6px;
        }

        .toggle .toggle-title h3 {
            font-size: 20px;
            margin: 0px;
            line-height: 1;
            cursor: pointer;
            font-weight: 200;
        }

        .toggle .toggle-inner {
            padding: 7px 25px 10px 25px;
            display: none;
            margin: -7px 0 6px;
        }

        .toggle .toggle-inner div {
            max-width: 100%;
        }

        .toggle .toggle-title .title-name {
            display: block;
            padding: 25px 25px 14px;
        }

        .toggle .toggle-title a i {
            font-size: 22px;
            margin-right: 5px;
        }

        .toggle .toggle-title i {
            position: absolute;
            background: url("http://arielbeninca.com/Storage/plus_minus.png") 0px -24px no-repeat;
            width: 24px;
            height: 24px;
            transition: all 0.3s ease;
            margin: 20px;
            right: 0;
        }

        .toggle .toggle-title.active i {
            background: url("http://arielbeninca.com/Storage/plus_minus.png") 0px 0px no-repeat;
        }
    </style>
@endsection