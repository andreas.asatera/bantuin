@extends('layouts.main')
@section('title', '| Tentang Kami')
@section('content')
    <section class="banner_area" style="min-height: 50px;">
        {{--<div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Tentang Kami</h2>
                    <div class="page_link">
                        <a href="{{ route('main') }}">Beranda</a>
                        <a href="#">Tentang Kami</a>
                    </div>
                </div>
            </div>
        </div>--}}
    </section>
    <section class="contact_area" style="padding-top: 60px">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center" style="border-right: 1px dotted #333;">
                    <h2>Apa itu Bantuin?</h2>
                    <h4 class="text-justify" style="font-size: 1.5rem">Sebuah Aplikasi Karya Anak Bangsa yang berisi katalog Jasa-Jasa yang dapat dipilih dengan mudah oleh para pencari jasa sehingga bisa langsung terhubung secara langsung dengan penyedia jasa yang dibutuhkan.</h4>
{{--                    <img src="{{ asset('storage/images/logo/bantuin.jpg') }}" alt="Bantuin" width="100%">--}}
                </div>
                <div class="col-lg-6 text-center">
                    <h2>Apa Tujuan Bantuin?</h2>
                    <h4 class="text-justify" style="font-size: 1.5rem">
                        <ol>
                            <li>Penyetaraan harga yaitu panduan harga jasa disertai gambaran kualitas jasa.</li>
                            <li>Mensejahterakan masyarakat karena adanya peluang penghasilan tambahan untuk masyarakat melalui fitur bantuin hero.</li>
                            <li>Penyedia Jasa semakin mudah ditemukan oleh Pencari Jasa.</li>
                        </ol>
                    </h4>
                    {{--                    <img src="{{ asset('storage/images/logo/goal.jpg') }}" alt="Goal Bantuin" width="100%">--}}
                </div>
            </div>
            <br>
            {{--<div class="row">

            </div>--}}
            <br>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Nilai-Nilai Perusahaan</h2>
                    <img src="{{ asset('storage/images/logo/corevalue.jpg') }}" class="img-fluid" alt="Core Value">
                </div>
            </div>
        </div>
    </section>
@endsection