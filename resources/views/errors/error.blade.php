@extends('layouts.main')
@section('title', '| Terjadi Kesalahan')
@section('content')
	<!--================Blog Categorie Area =================-->
	<section class="blog_categorie_area pb-0">
	</section>
	<!--================Blog Categorie Area =================-->

	<!--================Blog Area =================-->
	<section class="blog_area pb-0">
		<div class="container">
			<div class="text-center">
				<img src="{{ asset('storage/images/banner/error.jpg') }}" class="img-responsive" alt="Coming Soon" width="50%" height="auto">
			</div>
		</div>
	</section>
	<!--================Blog Area =================-->
@endsection