@extends('layouts.main')
@section('title', '| Pemberitahuan')
@section('content')
    <!--================Blog Categorie Area =================-->
    <section class="blog_categorie_area">
    </section>
    <!--================Blog Categorie Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-header">
                        @if($user->notifications->count())
                            <a class="genric-btn submit_btn circle pull-right hidden-sm" role="button"
                               href="{{ route('notifmarkasread') }}" style="position: absolute; right: 15px;">
                                Sudah Dibaca
                            </a>
                            <h2 class="text-center">
                                Pemberitahuan
                            </h2>
                            <a class="genric-btn submit_btn circle show-sm" role="button"
                               href="{{ route('notifmarkasread') }}" style="display: none">
                                Sudah Dibaca
                            </a>
                        @else
                            <h2 class="text-center">
                                Pemberitahuan
                            </h2>
                        @endif
                    </div>
                    <hr>
                </div>
            </div>
            @if($user->notifications->count())
                @php
                    setlocale(LC_TIME, 'id_ID');
                        $dates = $user->notifications->groupBy(function ($val){
                        return Carbon\Carbon::parse($val->created_at)->formatLocalized('%A, %d %B %Y');
                    });
                @endphp
                @foreach($dates as $date => $notifications)
                    <h5>{{ $date }}</h5>
                    <hr>
                    @foreach($notifications as $notification)
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ $notification->data['url'] }}">
                                    <h5 class=" pl-4" style="color: #777777">
                                        {!! $notification->data['message'] !!}
                                        <span class="pull-right">
                                            @if(empty($notification->read_at))
                                                <span class="btn btn-success btn-circle"></span>
                                            @endif
                                        </span>
                                        <br>
                                        <small>{{ $notification->created_at->format('H:i:s') }}</small>
                                    </h5>
                                </a>
                            </div>
                        </div>
                        <hr class="mt-1">
                    @endforeach
                @endforeach
            @else
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5>Tidak ada pemberitahuan</h5>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!--================Blog Area =================-->
@endsection
@section('customcss')
    <style>
        .btn-circle {
            width: 15px;
            height: 15px;
            padding: 4px 0;
            line-height: 1.428571429;
            border-radius: 50%;
        }
    </style>
@endsection