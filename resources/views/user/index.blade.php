@extends('layouts.user')
@section('title')
    | Profile {{ $user->name }}
@endsection
@section('isi')
    <div class="col-lg-9 posts-list">
        <div class="single-post row">
            <div class="col-lg-12">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('error') }}
                    </div>
                @endif
                @if (isset($user->alasan))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Verifikasi identitas ditolak: <b style="color: black">{{ $user->alasan }}</b>
                    </div>
                @endif
                @if (session('errors'))
                    @foreach(session('errors') as $error)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <div class="feature-img">
                    <h1>Profile</h1>
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="informasi-tab" data-toggle="tab" href="#informasi"
                               role="tab" aria-controls="informasi" aria-selected="true">Informasi Umum</a>
                            <a class="nav-item nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab"
                               aria-controls="password" aria-selected="false">Ubah Password</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="informasi" role="tabpanel"
                             aria-labelledby="informasi-tab">
                            <p style="color: #6c757d; margin-top: 16px;">Kelola informasi profil anda untuk mengontrol,
                                melindungi dan mengamankan akun.</p>
                            <form method="POST" action="{{ route('updateuser') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12 form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                           value="{{ $user->email }}" disabled>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="name">Nama:</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="{{ $user->name }}" value="{{ $user->name}}" disabled>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="nohp">No HP:</label>
                                    <input type="text" class="form-control" id="nohp" name="nohp"
                                           placeholder="{{ $user->nohp ? $user->nohp : '' }}" value="{{ $user->nohp}}"
                                           disabled>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="gender">Gender:</label>
                                    <select name="gender" class="form-control" id="gender" disabled>
                                        <option value="1" {{ $user->gender == 1 ? 'selected' : '' }}>
                                            Laki-laki
                                        </option>
                                        <option value="2" {{ $user->gender == 2 ? 'selected' : '' }}>
                                            Perempuan
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="datepicker">Tanggal Lahir</label>
                                    <div class="input-group date">
                                        <input type="text"
                                               class="form-control"
                                               id="datepicker"
                                               name="birthday"
                                               value="{{ date('d-m-Y', strtotime($user->birthday)) }}" disabled>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="provinsi">Provinsi:</label>
                                    <select name="provinsi" class="form-control" id="provinsi" disabled
                                            style="width: 100%">
                                        <option></option>
                                        @foreach($provinces as $province)
                                            <option value="{{ $province->id }}" {{ $province->id == $user->provinsi ? 'selected' : '' }}>{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="kabkota">Kabupaten/Kota:</label>
                                    <select name="kabkota" class="form-control" id="kabkota" disabled
                                            style="width: 100%">
                                        <option></option>
                                        @if (isset($user->provinsi) && isset($user->kabkota))
                                            @foreach(\Indonesia::findProvince($user->provinsi, ['cities'])->cities as $city)
                                                <option value="{{ $city->id }}" {{ $city->id == $user->kabkota ? 'selected' : '' }}>{{ $city->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="kecamatan">Kecamatan:</label>
                                    <select name="kecamatan" class="form-control" id="kecamatan" disabled
                                            style="width: 100%">
                                        <option></option>
                                        @if (isset($user->provinsi) && isset($user->kabkota) && isset($user->kecamatan))
                                            @foreach(\Indonesia::findCity($user->kabkota, ['districts'])->districts as $district)
                                                <option value="{{ $district->id }}" {{ $district->id == $user->kecamatan ? 'selected' : '' }}>{{ $district->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="desa">Desa:</label>
                                    <select name="desa" class="form-control" id="desa" disabled style="width: 100%">
                                        <option></option>
                                        @if (isset($user->provinsi) && isset($user->kabkota) && isset($user->kecamatan) && isset($user->desa))
                                            @foreach(\Indonesia::findDistrict($user->kecamatan, ['villages'])->villages as $village)
                                                <option value="{{ $village->id }}" {{ $village->id == $user->desa ? 'selected' : '' }}>{{ $village->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="alamat">Alamat Lengkap:</label>
                                    <textarea name="alamat" id="alamat" class="form-control"
                                              placeholder="Masukkan alamat lengkap"
                                              disabled>{{ !empty($user->alamat) ? $user->alamat : "" }}</textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="noidentitas">
                                        No. Identitas:
                                        @if($user->statusidentitas == 1)
                                            <span class="badge badge-danger">belum diverifikasi admin</span>
                                        @elseif($user->statusidentitas == 2)
                                            <span class="badge badge-primary">Identitas terverifikasi</span>
                                        @endif
                                        <br>
                                        <small>(KTP/SIM/Paspor)</small>
                                    </label>
                                    <input type="text" class="form-control"
                                           {{ $user->noidentitas? 'id=noidentitas' : '' }} name="noidentitas"
                                           placeholder="Masukkan nomor identitas"
                                           value="{{ $user->noidentitas }}" disabled>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="fotoidentitas">Foto <i>Selfie</i> dengan Tanda Identitas
                                        <small>(KTP/SIM/Paspor)</small>
                                    </label>
                                    @if(empty($user->fotoidentitas))
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-default btn-file p-0">
                                                                <input type="file" id="imgfile" name="fotoidentitas"
                                                                       style="display: none;"/>
                                                                <input id="imgInp" type="button" class="genric-btn disable radius" value="Pilih Foto" onclick="document.getElementById('imgfile').click();" style="border: 1px solid #ced4da;" disabled/>
                                                            </span>
                                                        </span>
                                    @endif
                                    <br><img {{ $user->fotoidentitas ? "src=".asset('storage/images/identitas/').'/'.$user->fotoidentitas : '' }} alt=""
                                         id="img-upload">
                                </div>
                                <div class="col-md-12 text-center">
                                    <button class="genric-btn info radius" id="btnChange">UBAH</button>
                                    <button class="genric-btn danger-border" id="btnCancel" style="display: none">
                                        BATAL
                                    </button>
                                    <button type="submit" class="genric-btn info radius" id="btnSubmit"
                                            style="display: none">SIMPAN
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                            <p style="color: #6c757d; margin-top: 16px;">Untuk keamanan akun Anda, mohon untuk tidak
                                menyebarkan password Anda ke orang lain.</p>
                            @if (session('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form method="POST" action="{{ route('updatepass') }}">
                                @csrf
                                <table style="table-layout: auto; border: 0; width: 100%;">
                                    <tr>
                                        <td style="width:1%; white-space:nowrap;">Password saat ini</td>
                                        <td><label for="oldpw"></label><input type="password" class="form-control"
                                                                              id="oldpw" name="oldpw" value=""
                                                                              required="" style="margin-left: 8px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:1%; white-space:nowrap;">Password baru</td>
                                        <td><label for="newpw"></label><input type="password" class="form-control"
                                                                              id="newpw" name="newpw" value=""
                                                                              required="" style="margin-left: 8px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:1%; white-space:nowrap;">Konfirmasi password</td>
                                        <td><label for="newpw_confirmation"></label><input type="password"
                                                                                           class="form-control"
                                                                                           id="newpw_confirmation"
                                                                                           name="newpw_confirmation"
                                                                                           value="" required=""
                                                                                           style="margin-left: 8px;">
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-top: 16px"></div>
                                <button type="submit" value="submit" class="genric-btn info radius" id="btnSubmitPass"
                                        style="margin:0 auto; display:block;">SAVE
                                </button>
                            </form>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".updatepp").on('submit', function (event) {
                event.preventDefault();
                let formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{ route('updatepp') }}',
                    method: "POST",
                    data: formData,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        let $message = $('#message');
                        $message.css('display', 'block');
                        $message.html(data.message);
                        $message.removeClass().addClass(data.class);
                        if (data.status === 1) {
                            $message.delay(2000).queue(function (next) {
                                $(this).css('display', 'none');
                                next();
                            });
                            setTimeout(function () {
                                location.reload();
                            }, 2500);
                        } else {

                            $message.delay(3000).queue(function (next) {
                                $(this).css('display', 'none');
                                next();
                            });
                        }
                    }
                });
            });
            let btnChange = $("#btnChange");
            let btnCancel = $("#btnCancel");
            let btnSubmit = $("#btnSubmit");

            btnChange.click(function (e) {
                e.preventDefault();
                $('input').not('[name=_token], #email, #nohp, #noidentitas, #imgfile').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
                $('#imgInp').prop('disabled', false).addClass('info').removeClass('disable').css('border', "1px solid #38a4ff");
                $('select').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
                $('textarea').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
                btnChange.toggle();
                btnCancel.toggle();
                btnSubmit.toggle();
            });
            btnCancel.click(function (e) {
                e.preventDefault();
                btnChange.toggle();
                btnCancel.toggle();
                btnSubmit.toggle();
                $('input').not('[name=_token], #email, #nohp, #noidentitas, #imgfile').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
                $('#imgfile').val('');
                @if(!isset($user->fotoidentitas))
                $('#img-upload').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
                @endif
                $('#imgInp').prop('disabled', true).addClass('disable').removeClass('info').css('border', '1px solid #ced4da');
                $('select').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
                $('textarea').each(function () {
                    if ($(this).attr('disabled')) {
                        $(this).removeAttr('disabled');
                    } else {
                        $(this).attr({
                            'disabled': 'disabled'
                        });
                    }
                });
            });
            let url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var date_input = $('input[name="birthday"]'); //our date input has the name "birthday"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'dd-mm-yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
            var $provinsi = $('#provinsi');
            var $kabkota = $('#kabkota');
            var $kecamatan = $('#kecamatan');
            var $desa = $('#desa');
            $($provinsi).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih provinsi',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-provinsi') }}/" + $provinsi.val(),
                    type: 'GET',
                    success: function (data) {
                        $kabkota.empty();
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.cities, function (value, key) {
                            $kabkota.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kabkota.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');

            $($kabkota).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kabupaten/Kota',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kabkota') }}/" + $kabkota.val(),
                    type: 'GET',
                    success: function (data) {
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.districts, function (value, key) {
                            $kecamatan.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kecamatan.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');

            $($kecamatan).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kecamatan',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kecamatan') }}/" + $kecamatan.val(),
                    type: 'GET',
                    success: function (data) {
                        $desa.empty();
                        $.each(data.villages, function (value, key) {
                            $desa.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $desa.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');

            $($desa).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Desa',
                dropdownAutoWidth: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.btn-file :file').on('fileselect', function (event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').css('display', 'block');
                        $('#img-upload').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgfile").change(function () {
                readURL(this);
            });
        });
    </script>
@endsection
@section('customcss')
    <style type="text/css">
        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload {
            max-height: 250px;
            width: auto;
        }
        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
        }
        /* Modal Content */
        .modal-content-2 {
            position: relative;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
        }
        /* The Close Button */
        .close2 {
            color: white;
            position: absolute;
            top: 10px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }
        .close2:hover,
        .close2:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
@endsection