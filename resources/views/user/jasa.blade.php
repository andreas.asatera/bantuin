@extends('layouts.user')
@section('title')
    | Jasa-Ku
@endsection
@section('isi')
    <div class="col-lg-9 posts-list">
        <div class="single-post row">
            <div class="col-lg-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('error') !!}
                    </div>
                @endif
                <div class="feature-img">
                    @if (isset($jasa) && $jasa->verified != 3)
                        <span class="h1 align-middle">Jasa-Ku: <b>{{ $jasa->name }}</b></span>
                        @if ($jasa->product->where('status', '<>', '4')->count() < 3 && $jasa->verified == 1)
                            <span class="h3 float-right">
                                <a href="#" class="badge badge-primary align-middle" data-toggle="modal"
                                   data-target="#tambahjasa"><i class="fas fa-plus"></i> Tambah Layanan</a>
                            </span>
                        @endif
                        @if ($jasa->verified == 1)
                            @if ($jasa->product->count() == 0)
                                <hr>
                                <p style="color: #6c757d; margin-top: 16px;">Layanan masih kosong.<br></p>
                            @endif
                            @if ($jasa->product->count())
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        @php
                                            $tab = true;
                                        @endphp
                                        @foreach ($jasa->product as $product)
                                            <a class="nav-item nav-link {{ $tab ? 'active show' : '' }}"
                                               id="nav-{{ $product->id }}-{{ str_replace(' ', '', $product->name) }}-tab"
                                               data-toggle="tab"
                                               href="#nav-{{ $product->id }}-{{ str_replace(' ', '', $product->name) }}"
                                               role="tab"
                                               aria-controls="nav-{{ $product->id }}-{{ str_replace(' ', '', $product->name) }}"
                                               aria-selected="{{ $tab ? 'true' : 'false' }}">{{ $product->name }}</a>
                                            @php
                                                $tab = false;
                                            @endphp
                                        @endforeach
                                    </div>
                                    <div class="tab-content" id="nav-tabContent">
                                        @php
                                            $prd = true;
                                        @endphp
                                        @foreach ($jasa->product as $product)
                                            <div class="tab-pane fade {{ $prd ? 'show active' : '' }}"
                                                 id="nav-{{ $product->id }}-{{ str_replace(' ', '', $product->name) }}"
                                                 role="tabpanel"
                                                 aria-labelledby="nav-{{ $product->id }}-{{ str_replace(' ', '', $product->name) }}-tab">
                                                <div class="row">
                                                    <div class="col-md-2">Nama</div>
                                                    <div class="col-md-10">: {{ $product->name }}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">Biaya Layanan</div>
                                                    <div class="col-md-10">:
                                                        Rp. {{ number_format($product->price,0,',','.') }}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">Detail</div>
                                                    <div class="col-md-10">: {{ $product->detail }}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">Foto Layanan</div>
                                                    <div class="col-md-10">
                                                        :
                                                        @php
                                                            $images = explode("|", $product->image);
                                                        @endphp
                                                        @foreach ($images as $image)
                                                            <img class="img-fluid"
                                                                 src="{{ asset("storage/images/product/").'/'.$image}}"
                                                                 alt="" width="300" height="350">
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="row justify-content-center mt-2">
                                                    <button type="button" class="btn btn-primary mr-1"
                                                            data-toggle="modal"
                                                            data-target="#editLayanan-{{ $product->id }}">
                                                        Ubah
                                                    </button>
                                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#hapusLayanan-{{ $product->id }}">
                                                        Hapus Layanan
                                                    </button>
                                                </div>
                                                <div class="modal fade" id="editLayanan-{{ $product->id }}">
                                                    <form action="{{ route('product.update', $product->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">

                                                                <!-- Modal Header -->
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Ubah Layanan</h4>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>

                                                                <!-- Modal body -->
                                                                <div class="modal-body">
                                                                    <div class="col-md-12 form-group">
                                                                        <label for="jasa">Nama Jasa</label>
                                                                        <input class="form-control" id="jasa"
                                                                               name="jasa"
                                                                               onblur="this.placeholder = 'Nama Jasa'"
                                                                               onfocus="this.placeholder = ''"
                                                                               placeholder="Nama Jasa" required
                                                                               type="text"
                                                                               value="{{ $product->name }}"/>
                                                                    </div>
                                                                    <div class="col-md-12 form-group">
                                                                        <label for="price">Biaya Layanan</label>
                                                                        <input
                                                                                type="number" class="form-control"
                                                                                min="0"
                                                                                step="100"
                                                                                id="price" name="price"
                                                                                placeholder="Masukkan biaya layanan"
                                                                                value="{{ $product->price }}"
                                                                                required>
                                                                    </div>
                                                                    <div class="col-md-12 form-group">
                                                                        Kategori:<br><label
                                                                                for="selcat"></label><select
                                                                                name="category" id="selcat"
                                                                                class="nice-select wide">
                                                                            @foreach ($categoryList as $parent)
                                                                                <optgroup
                                                                                        label="{{ $parent->name }}"
                                                                                        data-i="{{ $parent->id }}">
                                                                                    @foreach ($parent->children as $category)
                                                                                        <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                                                    @endforeach
                                                                                </optgroup>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-12 form-group">
                                                                        Detail Layanan: <br>
                                                                        <textarea class="form-control"
                                                                                  placeholder="Detail Layanan"
                                                                                  onfocus="this.placeholder = ''"
                                                                                  onblur="this.placeholder = 'Detail Layanan'"
                                                                                  required
                                                                                  name="detail">{{ $product->detail }}</textarea>
                                                                    </div>
                                                                    {{--<div class="col-md-12 form-group">
                                                                        edit gambar on progress
                                                                    </div>--}}
                                                                </div>

                                                                <!-- Modal footer -->
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            data-dismiss="modal">Kembali
                                                                    </button>
                                                                    @if($product->status == 1)
                                                                        <button type="submit" name="action"
                                                                                value="2"
                                                                                class="btn btn-outline-danger">
                                                                            Nonaktifkan layanan
                                                                        </button>
                                                                    @endif
                                                                    @if($product->status == 2)
                                                                        <button type="submit" name="action"
                                                                                value="3"
                                                                                class="btn btn-outline-success">
                                                                            Aktifkan layanan
                                                                        </button>
                                                                    @endif
                                                                    <button type="submit" name="action" value="1"
                                                                            class="btn btn-primary">Ubah
                                                                        Layanan
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal fade" id="hapusLayanan-{{ $product->id }}">
                                                    <form action="{{ route('product.update', $product->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">

                                                                <!-- Modal Header -->
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Hapus Layanan</h4>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>

                                                                <!-- Modal body -->
                                                                <div class="modal-body">
                                                                    <div class="col-md-12 form-group">
                                                                        Apakah kamu yakin untuk menghapus layanan
                                                                        <b>{{ $product->name }}</b>?
                                                                    </div>
                                                                </div>

                                                                <!-- Modal footer -->
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            data-dismiss="modal">Kembali
                                                                    </button>
                                                                    <button type="submit" name="action" value="4"
                                                                            class="btn btn-danger">Hapus Layanan
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @php
                                                $prd = false;
                                            @endphp
                                        @endforeach
                                    </div>
                                </nav>
                            @endif
                        @elseif ($jasa->verified == 2)
                            <hr>
                            Jasa <b>{{ $jasa->name }}</b> telah di ban oleh admin.<br>
                            Hubungi admin untuk informasi selengkapnya.
                        @elseif ($jasa->verified == 0)
                            <hr>
                            Jasa <b>{{ $jasa->name }}</b> belum diverifikasi oleh admin.
                        @endif
                    @else
                        <form method="POST" action="{{ route('user-create-jasa') }}" enctype="multipart/form-data" id="formJasa">
                            @if(isset($jasa) && $jasa->verified ==3)
                                <hr>
                                <div class="alert alert-danger" role="alert">
                                    Pengajuan jasa ditolak oleh admin: <b style="color: black">{{ $jasa->alasan }}</b>
                                </div>
                                <label>
                                    <input type="text" name="id" value="{{ $jasa->id }}" hidden>
                                </label>
                            @endif
                            <h3>Buat Jasa-ku:</h3>
                            @csrf
                            <div class="col-md-12 form-group">
                                <label for="jasa">Nama Jasa</label>
                                <input class="form-control" id="jasa" name="jasa"
                                       onblur="this.placeholder = 'Nama Jasa'"
                                       onfocus="this.placeholder = ''"
                                       placeholder="Nama Jasa" required type="text"/>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="deskripsi">Deskripsi Jasa</label>
                                <textarea id="deskripsi" class="form-control" placeholder="Deskripsi jasa"
                                          onfocus="this.placeholder = ''"
                                          onblur="this.placeholder = 'Deskripsi jasa'" required
                                          name="deskripsi"></textarea>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="provinsi">Provinsi:</label>
                                <select name="provinsi" class="form-control" id="provinsi" style="width: 100%" required>
                                    <option></option>
                                    @foreach(\Indonesia::allProvinces() as $province)
                                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="kabkota">Kabupaten/Kota:</label>
                                <select name="kabkota" class="form-control" id="kabkota" style="width: 100%" required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="kecamatan">Kecamatan:</label>
                                <select name="kecamatan" class="form-control" id="kecamatan" style="width: 100%"
                                        required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="desa">Desa:</label>
                                <select name="desa" class="form-control" id="desa" style="width: 100%" required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="alamat">Alamat Lengkap:</label>
                                <textarea name="alamat" id="alamat" class="form-control"
                                          placeholder="Masukkan alamat lengkap" required></textarea>
                            </div>
                            <div class="col-md-12 form-group">
{{--                                <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>--}}
                                <div class="custom-file">
                                    <input id="image" type="file" class="custom-file-input" name="image[]"
                                           multiple required>
                                    <label for="image" class="custom-file-label text-truncate">Choose file...</label>
                                    <small>* maksimum 5 gambar dengan resolusi 300 pixel x 350 pixel dan ukuran gambar 5
                                        MB
                                    </small>
                                </div>
                            </div>
                            <button type="submit" value="submit" class="genric-btn info radius" id="btnSubmit"
                                    style="margin:0 auto; display:block;">BUAT JASA
                            </button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if (isset($jasa))
        <div class="modal fade" id="tambahjasa" role="dialog" aria-labelledby="tambahjasaLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tambahjasaLabel">Tambah Layanan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="login_form" action="{{ route('product.store') }}" method="post"
                          enctype="multipart/form-data">
                        <div class="modal-body">
                            @csrf
                            <div class="col-md-12 form-group">
                                <label for="name">Nama Layanan</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Masukkan nama" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="price">Biaya Layanan (mulai dari)</label>
                                <input type="number" class="form-control"
                                       min="0" step="100"
                                       id="price" name="price"
                                       placeholder="Masukkan biaya layanan"
                                       required>
                            </div>
                            <div class="col-md-12 form-group">
                                Kategori:<br><label for="selcat"></label><select name="category" id="selcat"
                                                                                 class="nice-select wide">
                                    @foreach ($categoryList as $parent)
                                        <optgroup label="{{ $parent->name }}" data-i="{{ $parent->id }}">
                                            @foreach ($parent->children as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                Detail Layanan: <br>
                                <textarea class="form-control" placeholder="Detail Layanan"
                                          onfocus="this.placeholder = ''"
                                          onblur="this.placeholder = 'Detail Layanan'" required
                                          name="detail"></textarea>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="custom-file">
                                    <input id="image" type="file" class="custom-file-input" name="image[]"
                                           multiple required>
                                    <label for="image" class="custom-file-label text-truncate">Choose file...</label>
                                    <small>* maksimum 5 gambar dengan ukuran masing-masing 4 MB</small>
                                </div>
                            </div>
                            <label>
                                <input type="text" name="jasaid" value=" {{ $jasa->id }}" hidden>
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="genric-btn danger-border" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="genric-btn info radius">Tambah Layanan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('customjs')
{{--    <script src="{{ asset('js/dropzone.js') }}"></script>--}}
    <script type="text/javascript">
        // Dropzone.autoDiscover = false;
        $(document).ready(function () {
            /*$("div#my-awesome-dropzone").dropzone({
                url: "route('user-imgupload') }}",
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 5,
                maxFiles: 5,
                maxFilesize: 5,
                acceptedFiles: 'image/!*',
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': "csrf_token() }}"
                },
                init: function() {
                    dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

                    // for Dropzone to process the queue (instead of default form behavior):
                    document.getElementById("btnSubmit").addEventListener("click", function(e) {
                        // Make sure that the form isn't actually being sent.
                        e.preventDefault();
                        e.stopPropagation();
                        dzClosure.processQueue();
                    });

                    //send all the form data along with the files:
                    this.on("sendingmultiple", function(data, xhr, formData) {
                        formData.append("jasa", jQuery("#jasa").val());
                        formData.append("deskripsi", jQuery("#deskripsi").val());
                    });
                }
                /!*success: function (file, response) {
                    var imgName = JSON.stringify(response);
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                    $("#formJasa").append($('<input type="hidden" ' +
                        'name="files[]" ' +
                        'value="' + response.success + '">'));
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                    console.log(response);
                }*!/
            });*/
            $('.custom-file-input').on("change", function () {
                let filenames = [];
                let files = document.getElementById("image").files;
                if (files.length > 5) {
                    alert("Gambar maksimum 5 file");
                    $(this)
                        .next(".custom-file-label")
                        .html("Choose file...");
                } else {
                    if (files.length > 1) {
                        filenames.push("Total Files (" + files.length + ")");
                    } else {
                        for (let i in files) {
                            if (files.hasOwnProperty(i)) {
                                filenames.push(files[i].name);
                            }
                        }
                    }
                    if (files.length > 5) {
                        alert("Maksimum 5 gambar");
                    }
                    $(this)
                        .next(".custom-file-label")
                        .html(filenames.join(","));
                }
            });
            var $provinsi = $('#provinsi');
            var $kabkota = $('#kabkota');
            var $kecamatan = $('#kecamatan');
            var $desa = $('#desa');
            $($provinsi).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih provinsi',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-provinsi') }}/" + $provinsi.val(),
                    type: 'GET',
                    success: function (data) {
                        $kabkota.empty();
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.cities, function (value, key) {
                            $kabkota.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kabkota.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($kabkota).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kabupaten/Kota',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kabkota') }}/" + $kabkota.val(),
                    type: 'GET',
                    success: function (data) {
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.districts, function (value, key) {
                            $kecamatan.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kecamatan.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($kecamatan).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kecamatan',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kecamatan') }}/" + $kecamatan.val(),
                    type: 'GET',
                    success: function (data) {
                        $desa.empty();
                        $.each(data.villages, function (value, key) {
                            $desa.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $desa.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($desa).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Desa',
                dropdownAutoWidth: true
            });
        });
    </script>
@endsection
@section('customcss')
    <style>
        .nice-select .list { max-height: 300px; overflow: scroll; }
    </style>
@endsection
