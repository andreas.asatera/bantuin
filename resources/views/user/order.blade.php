@extends('layouts.user')
@section('title', '| Jasa-Ku')
@section('isi')
    <div class="col-lg-9">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12 col-md-12 blog_details">
                            <div class="d-flex justify-content-center">
                                <h2>Pesanan Jasa</h2>
                            </div>
                            <nav>
                                <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active show"
                                       id="nav-pending-tab" data-toggle="tab"
                                       href="#nav-pending" role="tab"
                                       aria-controls="nav-pending"
                                       aria-selected="true" style="color: inherit">Belum Dikonfirmasi</a>
                                    <a class="nav-item nav-link"
                                       id="nav-checkpayment-tab" data-toggle="tab"
                                       href="#nav-checkpayment" role="tab"
                                       aria-controls="nav-checkpayment"
                                       aria-selected="false" style="color: inherit">Menunggu pembayaran</a>
                                    <a class="nav-item nav-link"
                                       id="nav-onprogress-tab" data-toggle="tab"
                                       href="#nav-onprogress" role="tab"
                                       aria-controls="nav-onprogress"
                                       aria-selected="false" style="color: inherit">Dalam Pengerjaan</a>
                                    <a class="nav-item nav-link"
                                       id="nav-done-tab" data-toggle="tab"
                                       href="#nav-done" role="tab"
                                       aria-controls="nav-done"
                                       aria-selected="false" style="color: inherit">Pengerjaan Selesai</a>
                                    <a class="nav-item nav-link"
                                       id="nav-history-tab" data-toggle="tab"
                                       href="#nav-history" role="tab"
                                       aria-controls="nav-history"
                                       aria-selected="false" style="color: inherit">Riwayat Pesanan</a>
                                </div>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade active show"
                                         id="nav-pending" role="tabpanel"
                                         aria-labelledby="nav-pending-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No. Pemesanan</th>
                                                    <th scope="col">Jasa</th>
                                                    <th scope="col">Catatan Pemesanan</th>
                                                    <th scope="col">Tanggal Pemesanan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders->where('status', 1)->count())
                                                    @foreach($orders as $order)
                                                        @if($order->status == 1)
                                                            <tr>
                                                                <td>{{ $order->ref }}</td>
                                                                <td>{{ $order->product->name }}</td>
                                                                <td>
                                                                    @if(isset($order->note))
                                                                        {{$order->note}}
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>{{ date('d F Y, H:i:s', strtotime($order->created_at)) }}</td>
                                                                <td><a href="#" data-toggle="modal"
                                                                       data-target="#batal-{{$order->ref}}"
                                                                       style="color: inherit"><i
                                                                                class="fa fa-trash" aria-hidden="true"
                                                                                title="Batalkan pesanan"></i></a></td>
                                                            </tr>
                                                            <div class="modal fade" id="batal-{{$order->ref}}"
                                                                 tabindex="-1" role="dialog"
                                                                 aria-labelledby="batal-{{$order->ref}}-Label"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <form class="login_form"
                                                                              action="{{ route('user-order-cancel') }}"
                                                                              method="post"
                                                                              enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="batal-{{$order->ref}}-Label">
                                                                                    Batalkan Pesanan</h5>
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                @csrf
                                                                                <div class="col-md-12 form-group">
                                                                                    <h4>Batalkan
                                                                                        pesanan {{$order->product->name}}
                                                                                        tanggal {{ date('d F Y, H:i:s', strtotime($order->created_at)) }}
                                                                                        ?</h4>
                                                                                </div>
                                                                                <input type="text" name="orderid"
                                                                                       value=" {{ $order->id }}"
                                                                                       hidden="true">
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                        class="genric-btn danger-border"
                                                                                        data-dismiss="modal">Kembali
                                                                                </button>
                                                                                <button type="submit"
                                                                                        class="genric-btn info radius">
                                                                                    Batalkan Pesanan
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <th colspan="6" style="text-align: center">Anda tidak memiliki
                                                        pesanan
                                                        jasa
                                                    </th>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade"
                                         id="nav-checkpayment" role="tabpanel"
                                         aria-labelledby="nav-checkpayment-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No. Pemesanan</th>
                                                    <th scope="col">Nama Pemesan</th>
                                                    <th scope="col">No HP Pemesan</th>
                                                    <th scope="col">Jasa</th>
                                                    <th scope="col">Tanggal Pengerjaan</th>
                                                    <th scope="col">Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders->where('status', 2)->count())
                                                    @foreach($orders as $order)
                                                        @if($order->status == 2 && ($order->payment->status == '1' || $order->payment->status == '2'))
                                                            <tr>
                                                                <td>{{ $order->ref }}</td>
                                                                <td>{{ $order->user->name }}</td>
                                                                <td>{{ $order->user->nohp }}</td>
                                                                <td>{{ $order->product->name }}</td>
                                                                <td>{{ date('d F Y, H:i:s', strtotime($order->updated_at)) }}</td>
                                                                @if($order->payment->status == '1')
                                                                    <td><a href="#" data-toggle="modal"
                                                                           data-target="#bayar-{{$order->ref}}"
                                                                           class="genric-btn info-border radius">Bayar</a>
                                                                    </td>
                                                                @elseif($order->payment->status == '2')
                                                                    <td>Menunggu verifikasi pembayaran dari admin</td>
                                                                @endif
                                                            </tr>
                                                            <div class="modal fade" id="bayar-{{$order->ref}}"
                                                                 tabindex="-1" role="dialog"
                                                                 aria-labelledby="bayar-{{$order->ref}}-Label"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <form class="login_form"
                                                                              action="{{ route('userpayment.store') }}"
                                                                              method="post"
                                                                              enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="bayar-{{$order->ref}}-Label">
                                                                                    Bayar Pesanan</h5>
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                @csrf
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Nama Jasa
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        : {{ $order->product->name }}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Biaya Jasa
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        :
                                                                                        Rp. {{ number_format($order->product->price,0,',','.') }}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Kode Unik
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        : {{ $order->payment->nominal - $order->product->price }}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Total yang harus dibayarkan
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        :
                                                                                        Rp. {{ number_format($order->payment->nominal,0,',','.') }}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        No. Rekening Bantuin <br>
                                                                                        <small>(BCA)</small>
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        1400764642<br>an. Andreas
                                                                                        Asatera Sandi Nofa
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Nama Pemilik Rekening
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        <input type="text" name="nama"
                                                                                               class="form-control"
                                                                                               id="nama" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        No. Rekening
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        <input type="text" name="norek"
                                                                                               class="form-control"
                                                                                               id="norek" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 form-group">
                                                                                        Foto Bukti Transfer
                                                                                    </div>
                                                                                    <div class="col-md-8 form-group">
                                                                                        <div class="custom-file">
                                                                                            <input id="image"
                                                                                                   type="file"
                                                                                                   class="custom-file-input"
                                                                                                   name="bukti"
                                                                                                   required>
                                                                                            <label for="image"
                                                                                                   class="custom-file-label text-truncate">Choose
                                                                                                file...</label>
                                                                                            <small>* maksimum ukuran
                                                                                                gambar 5 MB
                                                                                            </small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" name="orderid"
                                                                                       value=" {{ $order->id }}"
                                                                                       hidden="true">
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                        class="genric-btn danger-border"
                                                                                        data-dismiss="modal">Kembali
                                                                                </button>
                                                                                <button type="submit"
                                                                                        class="genric-btn info radius">
                                                                                    Bayar Pesanan
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <th colspan="6" style="text-align: center">Anda tidak memiliki
                                                        pesanan jasa yang belum dibayar
                                                    </th>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade"
                                         id="nav-onprogress" role="tabpanel"
                                         aria-labelledby="nav-onprogress-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No. Pemesanan</th>
                                                    <th scope="col">Nama Pemesan</th>
                                                    <th scope="col">No HP Pemesan</th>
                                                    <th scope="col">Jasa</th>
                                                    <th scope="col">Tanggal Pengerjaan</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders->where('status', 2)->count())
                                                    @foreach($orders as $order)
                                                        @if($order->status == 2 && $order->payment->status == '3')
                                                            <tr>
                                                                <td>{{ $order->ref }}</td>
                                                                <td>{{ $order->user->name }}</td>
                                                                <td>{{ $order->user->nohp }}</td>
                                                                <td>{{ $order->product->name }}</td>
                                                                <td>{{ date('d F Y, H:i:s', strtotime($order->updated_at)) }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <th colspan="6" style="text-align: center">Anda tidak memiliki
                                                        pesanan jasa yang belum dibayar
                                                    </th>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade"
                                         id="nav-done" role="tabpanel"
                                         aria-labelledby="nav-done-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No. Pemesanan</th>
                                                    <th scope="col">Nama Pemesan</th>
                                                    <th scope="col">No HP Pemesan</th>
                                                    <th scope="col">Jasa</th>
                                                    <th scope="col">Tanggal Penyelesaian</th>
                                                    <th scope="col">Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders->where('status', 4)->count())
                                                    @foreach($orders as $order)
                                                        @if($order->status == 4 && !isset($order->rating))
                                                            <tr>
                                                                <td>{{ $order->ref }}</td>
                                                                <td>{{ $order->user->name }}</td>
                                                                <td>{{ $order->user->nohp }}</td>
                                                                <td>{{ $order->product->name }}</td>
                                                                <td>{{ date('d F Y, H:i:s', strtotime($order->time_finish)) }}</td>
                                                                <td>
                                                                    <a href="#" data-toggle="modal"
                                                                       data-target="#selesai-{{$order->ref}}"
                                                                       style="color: inherit">
                                                                        <i class="fa fa-tasks" aria-hidden="true"
                                                                           title="Konfirmasi penyelesaian pesanan"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <div class="modal fade" id="selesai-{{$order->ref}}"
                                                                 tabindex="-1" role="dialog"
                                                                 aria-labelledby="selesai-{{$order->ref}}-Label"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <form class="login_form"
                                                                              action="{{ route('user-order-confirm') }}"
                                                                              method="post"
                                                                              enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="selesai-{{$order->ref}}-Label">
                                                                                    Konfirmasi Pengerjaan Jasa</h5>
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                @csrf
                                                                                <div class="col-md-12">
                                                                                    <p>Apakah pengerjaan
                                                                                        jasa {{ $order->product->name }}
                                                                                        pada
                                                                                        tanggal {{ date('d F Y, H:i:s', strtotime($order->updated_at)) }}
                                                                                        telah sesuai?</p>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <select id="konfirmasi"
                                                                                            name="konfirmasi">
                                                                                        <option value="1">Pengerjaan
                                                                                            telah sesuai
                                                                                        </option>
                                                                                        <option value="2">Pengerjaan
                                                                                            tidak sesuai
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-12" id="ratingdiv"
                                                                                     style="margin-top: 16px">
                                                                                    <label for="rating">Berikan rating
                                                                                        pengerjaan jasa:</label>
                                                                                    <select id="rating">
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-12" id="komplaindiv"
                                                                                     style="display: none;margin-top: 16px">
                                                                                    <label for="komplain">Kekurangan
                                                                                        dalam pengerjaan
                                                                                        jasa:</label><br>
                                                                                    <textarea type="text"
                                                                                              name="komplain"
                                                                                              id="komplain"
                                                                                              class="form-control"
                                                                                              disabled
                                                                                              required></textarea>
                                                                                    {{--<select id="komplain"
                                                                                            name="komplain[]" multiple
                                                                                            disabled>
                                                                                        <option value="1">keluhan 1
                                                                                        </option>
                                                                                        <option value="2">keluhan 2
                                                                                        </option>
                                                                                    </select>--}}
                                                                                </div>
                                                                                <div class="col-md-12" id="reviewdiv">
                                                                                    <label for="review">Berikan ulasan
                                                                                        pengerjaan jasa
                                                                                        <small>(opsional)</small>
                                                                                    </label>
                                                                                    <textarea type="text" name="review"
                                                                                              class="form-control"
                                                                                              id="review"></textarea>
                                                                                </div>
                                                                                <input type="text" id="rating-val"
                                                                                       name="rating" value="1" hidden>
                                                                                <input type="text" name="orderid"
                                                                                       value=" {{ $order->id }}"
                                                                                       hidden="true">
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                        class="genric-btn danger-border"
                                                                                        data-dismiss="modal">Kembali
                                                                                </button>
                                                                                <button type="submit"
                                                                                        class="genric-btn info radius">
                                                                                    Konfirmasi Pesanan
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <th colspan="6" style="text-align: center">Anda tidak memiliki
                                                        pesanan jasa yang telah diselesaikan
                                                    </th>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade"
                                         id="nav-history" role="tabpanel"
                                         aria-labelledby="nav-history-tab">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No. Pemesanan</th>
                                                    <th scope="col">Nama Pemesan</th>
                                                    <th scope="col">No HP Pemesan</th>
                                                    <th scope="col">Jasa</th>
                                                    <th scope="col">Tanggal Pemesanan</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders->where('status', 3)->count() || $orders->where('status', 4)->count() || $orders->where('status', 5)->count())
                                                    @foreach($orders as $order)
                                                        @if($order->status == 3 || $order->status == 4 || $order->status == 5)
                                                            <tr>
                                                                <td>{{ $order->ref }}</td>
                                                                <td>{{ $order->user->name }}</td>
                                                                <td>{{ $order->user->nohp }}</td>
                                                                <td>{{ $order->product->name }}</td>
                                                                <td>{{ date('d F Y, H:i:s', strtotime($order->created_at)) }}</td>
                                                                <td>
                                                                    @if(isset($order->rating) && $order->rating->status == '2')
                                                                        Terdapat keluhan pada pesanan jasa
                                                                    @elseif(isset($order->rating) && $order->rating->status == '1')
                                                                        @switch($order->status)
                                                                            @case(3)
                                                                            Pesanan ditolak oleh penyedia jasa
                                                                            @break
                                                                            @case(4)
                                                                            Pengerjaan telah selesai
                                                                            @break
                                                                            @case(5)
                                                                            Pemesanan dibatalkan
                                                                            @break
                                                                            @default
                                                                            @break
                                                                        @endswitch
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <th colspan="6" style="text-align: center">Anda tidak memiliki
                                                        riwayat pemesanan jasa
                                                    </th>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
    <script src="{{ asset('js/ui-choose.js') }}"></script>
    <script src="{{ asset('js/jquery.barrating.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#konfirmasi').ui_choose({
                click: function (value) {
                    if (value == 1) {
                        $('#ratingdiv').show();
                        $('#reviewdiv').show();
                        $('#review').prop("disabled", false);
                        $('#rating-val').prop("disabled", false);

                        $('#komplaindiv').hide();
                        $('#komplain').prop("disabled", true);
                    }
                    if (value == 2) {
                        $('#ratingdiv').hide();
                        $('#reviewdiv').hide();
                        $('#review').prop("disabled", true);
                        $('#rating-val').prop("disabled", true);

                        $('#komplaindiv').show();
                        $('#komplain').prop("disabled", false);
                    }
                }
            });
            // $('#komplain').ui_choose();
            $('#rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function (value, text, event) {
                    if (typeof (event) !== 'undefined') {
                        // rating was selected by a user
                        val = $(event.target).data("rating-value");
                        $("#rating-val").val(val);
                    }
                }
            });
        })
    </script>
    <script type="text/javascript">
        $('.custom-file-input').on("change", function () {
            let filenames = [];
            let files = document.getElementById("image").files;
            if (files.length > 1) {
                alert("Gambar maksimum 1 file");
                $(this)
                    .next(".custom-file-label")
                    .html("Choose file...");
            } else {
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                if (files.length > 1) {
                    alert("Maksimum 1 gambar");
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            }
        });
    </script>
@endsection
@section('customcss')
    <link rel="stylesheet" href="{{ asset('css/ui-choose.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-stars.css') }}">
@endsection