@extends('layouts.user')
@section('title')
    | Hero
@endsection
@section('isi')
    <div class="col-lg-9 posts-list">
        <div class="single-post row">
            <div class="col-lg-12">
                @if (session('status'))
                    <div class="alert {{ session('class') }}" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('status') !!}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! session('error') !!}
                    </div>
                @endif
                <div class="feature-img">
                    <span class="h1 align-middle">Bantuin Hero</span>
                    <hr>
                    @if(!isset($user->hero))
                        <h3>
                            Daftar sekarang sebagai <b>Bantuin Hero</b> dan dapatkan peluang pendapatan tambahan hingga <b>Rp. 175.000/hari atau Rp. 5.000.000/bulan</b>!!!
                        </h3>
                        <a class="main_btn" href="{{ route('hero.create') }}"
                           style="color: white; background: #F37320;">Daftar Sekarang</a>
                    @else
                        @if($user->hero == 1)
                            <h4>Pendaftaran sebagai <b>Bantuin Hero</b> sukses. Menunggu konfirmasi dari admin.</h4>
                        @elseif($user->hero == 2)
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active show"
                                       id="nav-add-customer-tab"
                                       data-toggle="tab"
                                       href="#nav-add-customer"
                                       role="tab"
                                       aria-controls="nav-add-customer"
                                       aria-selected="true">Customer</a>
                                    <a class="nav-item nav-link"
                                       id="nav-add-mitra-tab"
                                       data-toggle="tab"
                                       href="#nav-add-mitra"
                                       role="tab"
                                       aria-controls="nav-add-mitra"
                                       aria-selected="false">Mitra</a>
                                    {{--<a class="nav-item nav-link"
                                       id="nav-report-tab"
                                       data-toggle="tab"
                                       href="#nav-report"
                                       role="tab"
                                       aria-controls="nav-report"
                                       aria-selected="false">Laporan</a>--}}
                                </div>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active"
                                         id="nav-add-customer"
                                         role="tabpanel"
                                         aria-labelledby="nav-add-customer-tab">
                                        <h3 class="mt-2">
                                            {{ $customers->count() ? "Daftar Customer" : "Customer masih kosong"}}
                                            <span class="h5 float-right">
                                                <a href="#" class="main_btn" data-toggle="modal"
                                                   data-target="#tambahcustomer"
                                                   style="line-height: 24px; padding: 5px;">
                                                    <i class="fas fa-plus"></i> Tambah Customer
                                                </a>
                                            </span>
                                        </h3>
                                        <div class="modal fade" id="tambahcustomer">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah Customer</h4>
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="reg_form" style="padding: 0;">
                                                            <form class="row login_form"
                                                                  action="{{ route('hero.customer.store') }}"
                                                                  method="post" id="contactForm">
                                                                @csrf
                                                                <div class="col-md-12 form-group">
                                                                    <input type="text" class="form-control" id="name"
                                                                           name="name" value="{{ old('name') }}"
                                                                           required autofocus
                                                                           placeholder="Nama Lengkap">
                                                                    @if ($errors->has('name'))
                                                                        <strong>{{ $errors->first('name') }}</strong>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <input type="email" class="form-control" id="email"
                                                                           name="email"
                                                                           value="{{ old('email') }}" required
                                                                           placeholder="Alamat Email">
                                                                    @if ($errors->has('email'))
                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <input type="tel" class="form-control" id="nohp"
                                                                           name="nohp"
                                                                           value="{{ old('nohp') }}" required
                                                                           placeholder="Nomor HP">
                                                                    @if ($errors->has('nohp'))
                                                                        <strong>{{ $errors->first('nohp') }}</strong>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <select name="gender" class="nice-select wide">
                                                                        <option value="1" selected>Laki-laki</option>
                                                                        <option value="2">Perempuan</option>
                                                                    </select>
                                                                    @if ($errors->has('gender'))
                                                                        <strong>{{ $errors->first('gender') }}</strong>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <div class="input-group date">
                                                                        <input type="text" class="form-control"
                                                                               id="datepicker" autocomplete="off"
                                                                               name="birthday"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               placeholder="Tanggal Lahir">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text"><i
                                                                                        class="fas fa-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                    @if ($errors->has('birthday'))
                                                                        <strong>{{ $errors->first('birthday') }}</strong>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <input type="password" class="form-control"
                                                                           id="password" name="password" required
                                                                           placeholder="Kata Sandi"
                                                                           autocomplete="new-password">
                                                                    @if ($errors->has('password'))
                                                                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <input type="password" class="form-control"
                                                                           id="pass" name="password_confirmation"
                                                                           required placeholder="Ulang Kata Sandi">
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <button type="submit" value="submit"
                                                                            class="btn submit_btn">Buat Akun
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if($customers->count())
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>Alamat</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($customers as $customer)
                                                        <tr>
                                                            <td>{{ $customer->name }}</td>
                                                            <td>{{ $customer->email }}</td>
                                                            <td>{{ $customer->alamat? $customer->alamat : '-' }}</td>
                                                            <td>{{ $customer->hasVerifiedEmail() ? "Akun telah aktif" : "Akun belum verifikasi email" }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade"
                                         id="nav-add-mitra"
                                         role="tabpanel"
                                         aria-labelledby="nav-add-mitra-tab">
                                        <h3 class="mt-2">
                                            {{ $mitras->count() ? "Daftar Mitra" : "Mitra masih kosong"}}
                                            <span class="h5 float-right">
                                                <a href="#" class="main_btn" data-toggle="modal"
                                                   data-target="#tambahmitra"
                                                   style="line-height: 24px; padding: 5px;">
                                                    <i class="fas fa-plus"></i> Tambah Mitra
                                                </a>
                                            </span>
                                        </h3>
                                        <div class="modal fade" id="tambahmitra">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah Mitra</h4>
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                            <form method="POST" class="login_form" action="{{ route('hero.mitra.store') }}"
                                                                  enctype="multipart/form-data" id="formJasa">
                                                                @csrf
                                                                <div class="col-md-12 form-group">
                                                                    <label for="provinsi">Customer:</label>
                                                                    <select name="customer" class="form-control"
                                                                            id="customer" style="width: 100%" required>
                                                                        <option></option>
                                                                        @foreach($customers as $customer)
                                                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="noidentitas">
                                                                        No. Identitas:<br>
                                                                        <small>(KTP/SIM/Paspor)</small>
                                                                    </label>
                                                                    <input type="text" class="form-control"
                                                                           name="noidentitas"
                                                                           placeholder="Masukkan nomor identitas">
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="imgfile">Foto <i>Selfie</i> dengan
                                                                        Tanda Identitas
                                                                        <small>(KTP/SIM/Paspor)</small>
                                                                    </label><br>
                                                                    <span class="input-group-btn">
                                                            <span class="btn btn-default btn-file p-0">
                                                                <input type="file" id="imgfile" name="fotoidentitas"
                                                                       style="display: none;"/>
                                                                <input id="imgInp" type="button"
                                                                       class="btn submit_btn"
                                                                       value="Pilih Foto"
                                                                       onclick="document.getElementById('imgfile').click();"
                                                                       style="border: 1px solid #ced4da;"/>
                                                            </span>
                                                        </span>
                                                                    <br><img
                                                                            {{ isset($customer->fotoidentitas) ? "src=".asset('storage/images/identitas/').'/'.$customer->fotoidentitas : '' }} alt=""
                                                                            id="img-upload" style="display: none;width: inherit;height: inherit;">
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="jasa">Nama Jasa</label>
                                                                    <input class="form-control" id="jasa" name="jasa"
                                                                           onblur="this.placeholder = 'Nama Jasa'"
                                                                           onfocus="this.placeholder = ''"
                                                                           placeholder="Nama Jasa" required type="text"/>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="deskripsi">Deskripsi Jasa</label>
                                                                    <textarea id="deskripsi" class="form-control"
                                                                              placeholder="Deskripsi jasa"
                                                                              onfocus="this.placeholder = ''"
                                                                              onblur="this.placeholder = 'Deskripsi jasa'"
                                                                              required
                                                                              name="deskripsi"></textarea>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="provinsi">Provinsi:</label>
                                                                    <select name="provinsi" class="form-control"
                                                                            id="provinsi" style="width: 100%" required>
                                                                        <option></option>
                                                                        @foreach(\Indonesia::allProvinces() as $province)
                                                                            <option value="{{ $province->id }}">{{ $province->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="kabkota">Kabupaten/Kota:</label>
                                                                    <select name="kabkota" class="form-control" id="kabkota"
                                                                            style="width: 100%" required>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="kecamatan">Kecamatan:</label>
                                                                    <select name="kecamatan" class="form-control"
                                                                            id="kecamatan" style="width: 100%"
                                                                            required>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="desa">Desa:</label>
                                                                    <select name="desa" class="form-control" id="desa"
                                                                            style="width: 100%" required>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="alamat">Alamat Lengkap:</label>
                                                                    <textarea name="alamat" id="alamat" class="form-control"
                                                                              placeholder="Masukkan alamat lengkap"
                                                                              required></textarea>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    {{--                                <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>--}}
                                                                    <label for="fotojasa">Pilih foto jasa:</label>
                                                                    <div class="custom-file" id="fotojasa">
                                                                        <input id="image" type="file"
                                                                               class="custom-file-input" name="image[]"
                                                                               multiple required>
                                                                        <label for="image"
                                                                               class="custom-file-label text-truncate">Pilih foto...</label>
                                                                        <small>* maksimum 5 gambar dengan resolusi 300 pixel
                                                                            x 350 pixel dan ukuran gambar 5
                                                                            MB
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <button type="submit" value="submit"
                                                                            class="btn submit_btn">Tambah Mitra
                                                                    </button>
                                                                </div>
                                                            </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="tab-pane fade"
                                         id="nav-report"
                                         role="tabpanel"
                                         aria-labelledby="nav-add-report-tab">
                                        tes 3
                                    </div>--}}
                                </div>
                            </nav>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function () {
            var date_input = $('input[name="birthday"]'); //our date input has the name "birthday"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'dd-mm-yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);

            $('.custom-file-input').on("change", function () {
                let filenames = [];
                let files = document.getElementById("image").files;
                if (files.length > 5) {
                    alert("Gambar maksimum 5 file");
                    $(this)
                        .next(".custom-file-label")
                        .html("Choose file...");
                } else {
                    if (files.length > 1) {
                        filenames.push("Total Files (" + files.length + ")");
                    } else {
                        for (let i in files) {
                            if (files.hasOwnProperty(i)) {
                                filenames.push(files[i].name);
                            }
                        }
                    }
                    if (files.length > 5) {
                        alert("Maksimum 5 gambar");
                    }
                    $(this)
                        .next(".custom-file-label")
                        .html(filenames.join(","));
                }
            });
            var $customer = $('#customer');
            var $provinsi = $('#provinsi');
            var $kabkota = $('#kabkota');
            var $kecamatan = $('#kecamatan');
            var $desa = $('#desa');
            $($customer).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih customer',
                dropdownAutoWidth: true
            });
            $($provinsi).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih provinsi',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-provinsi') }}/" + $provinsi.val(),
                    type: 'GET',
                    success: function (data) {
                        $kabkota.empty();
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.cities, function (value, key) {
                            $kabkota.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kabkota.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($kabkota).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kabupaten/Kota',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kabkota') }}/" + $kabkota.val(),
                    type: 'GET',
                    success: function (data) {
                        $kecamatan.empty();
                        $desa.empty();
                        $.each(data.districts, function (value, key) {
                            $kecamatan.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $kecamatan.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($kecamatan).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Kecamatan',
                dropdownAutoWidth: true
            }).on('select2:select', function () {
                $.ajax({
                    url: "{{ route('ajax-address-kecamatan') }}/" + $kecamatan.val(),
                    type: 'GET',
                    success: function (data) {
                        $desa.empty();
                        $.each(data.villages, function (value, key) {
                            $desa.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                        $desa.prepend("<option value=''></option>").val('');
                    }
                });
            }).trigger('change');
            $($desa).select2({
                theme: 'bootstrap4',
                placeholder: 'Pilih Desa',
                dropdownAutoWidth: true
            });
            $('.btn-file :file').on('fileselect', function (event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').css('display', 'block');
                        $('#img-upload').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgfile").change(function () {
                readURL(this);
            });
        });
    </script>
@endsection
@section('customcss')
    <style>
        .nice-select .list {
            max-height: 300px;
            overflow: scroll;
        }
    </style>
@endsection