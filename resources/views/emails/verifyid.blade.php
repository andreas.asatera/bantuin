@component('mail::message')
Halo, Bantuin membutuhkanmu untuk melakukan verifikasi identitas dari pengguna. Berikut data pengguna yang membutuhkan verifikasi:
@component('mail::table')
    |ID Pengguna|Nama Pengguna|
    |:-:|:-:|
    |{{$user->id}}|{{$user->name}}|
@endcomponent
id:2019-05-24-22:41:08:970t
Klik tombol dibawah untuk melakukan verifikasi:
@component('mail::button', ['url' => route('admin-user-review', ['id'=>$user->id])])
Review User
@endcomponent
@endcomponent
