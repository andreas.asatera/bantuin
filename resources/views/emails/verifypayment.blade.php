@component('mail::message')
Halo, Bantuin membutuhkanmu untuk melakukan verifikasi pembayaran. Berikut data pembayaran yang membutuhkan verifikasi:
@component('mail::table')
    |ID Pembayaran|Nama Pengguna|Nama Product|
    |:-:|:-:|:-:|
    |{{$payment->id}}|{{$payment->user->name}}|{{$payment->order->product->name}}|
@endcomponent
Klik tombol dibawah untuk melakukan verifikasi:
@component('mail::button', ['url' => route('adminpayment.show', ['id'=>$payment->id])])
    Review Pembayaran
@endcomponent
@endcomponent
