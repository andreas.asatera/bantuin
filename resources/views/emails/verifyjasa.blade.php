@component('mail::message')
Halo, Bantuin membutuhkanmu untuk melakukan verifikasi jasa. Berikut data jasa yang membutuhkan verifikasi:
@component('mail::table')
    |ID Jasa|Nama Jasa|
    |:-:|:-:|
    |{{$jasa->id}}|{{$jasa->name}}|
@endcomponent
Klik tombol dibawah untuk melakukan verifikasi:
@component('mail::button', ['url' => route('admin-jasa-review', ['id'=>$jasa->id])])
    Review Jasa
@endcomponent
@endcomponent
