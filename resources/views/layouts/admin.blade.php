<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Panel | @yield('title')</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('adminlte/bower_components/jvectormap/jquery-jvectormap.css') }}">
	<link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
	<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	@yield('customcss')
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="{{ route('admin') }}" class="logo">
				<span class="logo-mini"><img src="{{ asset('fashiop/img/logo.png') }}" alt="" width="30px" height="auto"></span>
				<span class="logo-lg"><img src="{{ asset('fashiop/img/logo.png') }}" alt="" width="30px" height="auto"> Admin Panel</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="{{ route('main') }}">Bantuin</a>
						</li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{{ $user->pp ? asset("storage/images/pp/").'/'.$user->pp : asset("fashiop/img/blog/author.png")}}" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>@php
						$name = $user->name;
						$Split = explode(" ", $name);
						$SplitAt = 0;
						foreach($Split as $Word){
							$SplitAt+= strlen($Word)-1;
							if($SplitAt >= strlen($name)){
								break;
							}
						}
						$NewSentence = wordwrap($name, $SplitAt, '<br />' . PHP_EOL);
						echo $NewSentence;
					@endphp</p>
				</div>
			</div>
			<ul class="sidebar-menu" data-widget="tree">
				<li class="header">NAVIGATION</li>
				<li class="{{ Request::is('admin') ? 'active' : '' }}">
					<a href="{{ Request::is('admin') ? '#' : route('admin') }}">
						<i class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
				</li>
				<li class="treeview {{ Request::is('admin/user') ? 'active' : '' }}">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>User</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li {{ Request::is('admin/user') ? 'class=active' : '' }}><a href="{{ Request::is('admin/user') ? '#' : route('admin-user') }}"><i class="fa fa-circle-o"></i> <span>Review User</span></a></li>
						<li><a href="#"><i class="fa fa-circle-o"></i> <span>User 2</span></a></li>
					</ul>
				</li>
				<li class="treeview {{ Request::is('admin/jasa') ? 'active' : '' }}">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Jasa</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li {{ Request::is('admin/jasa') ? 'class=active' : '' }}><a href="{{ Request::is('admin/jasa') ? '#' : route('admin-jasa') }}"><i class="fa fa-circle-o"></i> <span>Review Jasa</span></a></li>
						<li><a href="#"><i class="fa fa-circle-o"></i> <span>Edit Jasa</span></a></li>
					</ul>
				</li>
				<li class="treeview {{ Request::is('admin/category') ? 'active' : '' }}">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Kategori</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li {{ Request::is('admin/category') ? 'class=active' : '' }}><a href="{{ Request::is('admin/category') ? '#' : route('admin-category') }}"><i class="fa fa-circle-o"></i> <span>Tambah Kategori</span></a></li>
						<li {{ Request::is('admin/category/edit') ? 'class=active' : '' }}><a href="{{ Request::is('admin/category/edit') ? '#' : route('admin-category-edit') }}"><i class="fa fa-circle-o"></i> <span>Edit Kategori</span></a></li>
					</ul>
				</li>
				<li class="treeview {{ Request::is('admin/adminpayment/*') ? 'active' : '' }}">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Pembayaran</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li {{ Request::is('admin/adminpayment') ? 'class=active' : '' }}><a href="{{ Request::is('admin/adminpayment') ? '#' : route('adminpayment.index') }}"><i class="fa fa-circle-o"></i> <span>Pembayaran</span></a></li>
					</ul>
				</li>
				<li class="treeview {{ Request::is('admin/site/*') ? 'active' : '' }}">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Site</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li {{ Request::is('admin/site') ? 'class=active' : '' }}><a href="{{ Request::is('admin/site') ? '#' : route('site.index') }}"><i class="fa fa-circle-o"></i> <span>Setelan Umum</span></a></li>
					</ul>
				</li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		@yield('content')
	</div>
</div>
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/chart.js/Chart.js') }}"></script>
@yield('customscript')
</body>
</html>
