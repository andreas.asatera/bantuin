<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="{{ asset('fashiop/img/favicon.png') }}" type="image/png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bantuin') }} @yield('title', '')</title>

    <link rel="stylesheet" href="{{ asset('fashiop/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/lightbox/simpleLightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/nice-select/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/animate-css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/vendors/jquery-ui/jquery-ui.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <link href="{{ asset('fashiop/css/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('fashiop/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('fashiop/css/responsive.css') }}">

    <!-- pace -->
    <script data-pace-options='{ "ajax": false }' src='{{ asset("js/pace.min.js") }}'></script>
    <link rel="stylesheet" href="{{ asset('css/pace.css') }}">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <style type="text/css">
        .searchbar {
            margin-top: auto;
            height: 60px;
            border-radius: 30px;
        }

        .search_input {
            color: #222222;
            border: 0;
            outline: 0;
            background: none;
            width: 150px;
            caret-color: transparent;
            line-height: 40px;
            transition: width 0.4s linear;
            padding: 0 10px;
        }

        #searchbar:hover > .search_input {
            padding: 0 10px;
            caret-color: red;
        }

        #searchbar:hover > .search_icon {
            background: white;
            color: #e74c3c;
        }

        .search_icon {
            height: 40px;
            width: 40px;
            float: right;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            color: #222222;
        }
    </style>
    @yield('customcss')
</head>

<body>

<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="/">
                    <img src="{{ asset('fashiop/img/logo.png') }}" alt="" width="75px" height="auto">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent" style="overflow-y: inherit">
                    <div class="row w-100">
                        <div class="col-lg-8 pr-0">
                            <ul class="nav navbar-nav center_nav pull-right">
                                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('main') }}">Beranda</a>
                                </li>
                                <li class="nav-item {{ Request::is('jasa') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('jasa') }}">Jasa</a>
                                </li>
                                <li class="nav-item {{ Request::is('news') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('news') }}">Berita</a>
                                </li>
                                <li class="nav-item {{ Request::is('gallery') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('gallery') }}">Galeri</a>
                                </li>
                                <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('about') }}">Tentang Kami</a>
                                </li>
                                <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('faq') }}">Bantuan</a>
                                </li>
                            </ul>
                        </div>
                        @php($agent = new \Jenssegers\Agent\Agent())
                        <div class="col-lg-4">
                            <ul class="nav navbar-nav navbar-right right_nav pull-right">
                                <li class="nav-item">
                                    <div class="container h-100">
                                        <div class="d-flex justify-content-center h-100">
                                            <div class="searchbar">
                                                <form action="{{ route('jasa-search') }}" id="searchbar" method="GET">
                                                    <input class="search_input" type="text" name="q"
                                                           placeholder="Cari Jasa">
                                                    <input type="submit" hidden>
                                                    <a class="search_icon"
                                                       onclick="document.getElementById('searchbar').submit()"><i
                                                                class="fa fa-search" aria-hidden="true"></i></a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @auth
                                    @if(!$agent->isMobile())
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"
                                               role="button"
                                               aria-haspopup="true" aria-expanded="false">
                                                AKUN
                                            </a>
                                            @if(Auth::user()->unreadNotifications->count())
                                                <a href="{{ route('pemberitahuan.index') }}"
                                                   class="badge badge-primary align-middle">{{ Auth::user()->unreadNotifications->count() }}</a>
                                            @endif
                                            <div class="dropdown-menu dropdown-menu-right" style="width: max-content">
                                                <div class="card-header">
                                                    <div class="text-center">
                                                        <a href="{{ route('user') }}">
                                                        <img class="author_img rounded-circle"
                                                             src="{{ Auth::user()->pp ? asset("storage/images/pp/").'/'.Auth::user()->pp : asset("fashiop/img/blog/author.png") }}"
                                                             alt="" width="100px" height="auto">
                                                        <h5 style="color: black">{{ Auth::user()->name }}</h5>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    @if(Auth::user()->level == 3)
                                                        <a href="{{ route('admin') }}" style="color: inherit">
                                                            <h5>Admin Panel</h5>
                                                        </a>
                                                        <hr style="height: unset; width: unset;" class="mb-2">
                                                    @endif
                                                    <a href="{{ route('user') }}" style="color: inherit">
                                                        <h5>Profil Saya</h5>
                                                    </a>
                                                    <hr style="height: unset; width: unset;" class="mb-2">
                                                    <a href="{{ route('pemberitahuan.index') }}"
                                                       style="color: inherit">
                                                        <h5>
                                                            Pemberitahuan
                                                            @if(Auth::user()->unreadNotifications->count())
                                                                <a href="{{ route('pemberitahuan.index') }}"
                                                                   class="badge badge-primary align-middle">{{ Auth::user()->unreadNotifications->count() }}</a>
                                                            @endif
                                                        </h5>
                                                    </a>
                                                    <hr style="height: unset; width: unset;" class="mb-2">
                                                    <a href="{{ route('user-order') }}" style="color: inherit">
                                                        <h5>Pesanan Saya</h5>
                                                    </a>
                                                    @if(isset(Auth::user()->jasa))
                                                        <hr style="height: unset; width: unset;" class="mb-2">
                                                        <a href="{{ route('order') }}" style="color: inherit">
                                                            <h5>Pesanan Jasa-Ku</h5>
                                                        </a>
                                                    @endif
                                                    <hr style="height: unset; width: unset;" class="mb-2">
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
								document.getElementById('logout-form').submit();" style="color: inherit">
                                                        <h5>Keluar</h5>
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}"
                                                          method="POST"
                                                          style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                    @else
                                        <li class="nav-item dropdown">
                                            <a href="#" class="nav-link"
                                               role="button"
                                               aria-haspopup="true" aria-expanded="false" data-toggle="modal"
                                               data-target="#user">
                                                AKUN
                                                @if(Auth::user()->unreadNotifications->count())
                                                    <span class="badge badge-primary align-middle">{{ Auth::user()->unreadNotifications->count() }}</span>
                                                @endif
                                            </a>
                                            <div class="modal fade" id="user" tabindex="-1" role="dialog"
                                                 aria-labelledby="userModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body text-center">
                                                            <ul class="list-inline">
                                                                @if(Auth::user()->level == 3)
                                                                    <li class="list-inline-item"><a
                                                                                href="{{ route('admin') }}"
                                                                                style="color: inherit">
                                                                            <h5>Admin Panel</h5>
                                                                        </a></li>
                                                                    <hr>
                                                                @endif
                                                                <li class="list-inline-item"><a
                                                                            href="{{ route('user') }}"
                                                                            style="color: inherit">
                                                                        <h5>Profil Saya</h5>
                                                                    </a></li>
                                                                <hr>
                                                                <li class="list-inline-item"><a
                                                                            href="{{ route('pemberitahuan.index') }}"
                                                                            style="color: inherit">
                                                                        <h5>Pemberitahuan
                                                                            @if(Auth::user()->unreadNotifications->count())
                                                                                <a href="#"
                                                                                   class="badge badge-primary align-middle">{{ Auth::user()->unreadNotifications->count() }}</a>
                                                                            @endif</h5>
                                                                    </a></li>
                                                                <hr>
                                                                <li class="list-inline-item"><a
                                                                            href="{{ route('user-order') }}"
                                                                            style="color: inherit">
                                                                        <h5>Pesanan Saya</h5>
                                                                    </a></li>
                                                                @if(isset(Auth::user()->jasa))
                                                                    <hr>
                                                                    <li class="list-inline-item"><a
                                                                                href="{{ route('order') }}"
                                                                                style="color: inherit">
                                                                            <h5>Pesanan Jasa-Ku</h5>
                                                                        </a></li>
                                                                @endif
                                                                <hr>
                                                                <li class="list-inline-item"><a
                                                                            href="{{ route('logout') }}"
                                                                            onclick="event.preventDefault();
								document.getElementById('logout-form').submit();" style="color: inherit">
                                                                        <h5>Keluar</h5>
                                                                    </a></li>
                                                                <form id="logout-form" action="{{ route('logout') }}"
                                                                      method="POST"
                                                                      style="display: none;">
                                                                    @csrf
                                                                </form>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @else
                                    <li class="nav-item">
                                        <a href="{{ route('login') }}" class="nav-link">
                                            MASUK
                                        </a>
                                    </li>
                                @endauth
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>

</header>
<!--================Header Menu Area =================-->
@yield('content')

<!--================ start footer Area  =================-->
<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6 class="footer_title" style="margin-bottom: 16px">MOTTO KAMI</h6>
                    <p class="text-justify">"Anda bantu kami, kami bantu Anda"<br>
                        Dengan Bantuin, semua mudah meminta Bantuan</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6 class="footer_title" style="margin-bottom: 16px">HUBUNGI KAMI</h6>
                    <p>
                        {{--                        <i class="fas fa-phone" aria-hidden="true"> 081</i><br>--}}
                        {{--                        <i class="fab fa-whatsapp" aria-hidden="true"> 081</i><br>--}}
                        <i class="far fa-envelope" aria-hidden="true"> info@bantuinjasa.com</i><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget f_social_wd">
                    <h6 class="footer_title" style="margin-bottom: 16px">SOSIAL MEDIA KAMI</h6>
                    <div class="f_social">
                        <a href="https://www.facebook.com/bantuin.jasa" target="_blank">
                            <i class="fab fa-facebook"> bantuin.jasa</i>
                        </a><br>
                        <a href="http://instagram.com/bantuin.jasa" target="_blank">
                            <i class="fab fa-instagram"> bantuin.jasa</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-bottom d-flex justify-content-between align-items-center">
            <p class="col-lg-12 footer-text text-center">
            </p>
        </div>
    </div>
</footer>
<!--================ End footer Area  =================-->

<script src="{{ asset('fashiop/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('fashiop/js/popper.js') }}"></script>
<script src="{{ asset('fashiop/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('fashiop/js/stellar.js') }}"></script>
<script src="{{ asset('fashiop/vendors/lightbox/simpleLightbox.min.js') }}"></script>
<script src="{{ asset('fashiop/vendors/nice-select/js/jquery.nice-select.js') }}"></script>
<script src="{{ asset('fashiop/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('fashiop/vendors/isotope/isotope-min.js') }}"></script>
<script src="{{ asset('fashiop/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('fashiop/js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('fashiop/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('fashiop/vendors/flipclock/timer.js') }}"></script>
<script src="{{ asset('fashiop/vendors/counter-up/jquery.counterup.js') }}"></script>
<script src="{{ asset('fashiop/js/mail-script.js') }}"></script>
<script src="{{ asset('fashiop/js/theme.js') }}"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@yield('customjs')
<script type="text/javascript">
    $(document).ready(function () {
        /*$('select').select2({
            theme: 'bootstrap4',
            dropdownAutoWidth : true
        });*/
        $('.nice-select').niceSelect();
        $('#user').appendTo("body")
    });
</script>
</body>
</html>