@extends('layouts.main')
@section('title')
    @yield('title')
@endsection
@section('content')
    <section class="blog_categorie_area"></section>
    <!--================Blog Area =================-->
    <section class="blog_area single-post-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget author_widget">
                            <div class="imagewrap">
                                <img class="author_img rounded-circle"
                                     src="{{ $user->pp ? asset("storage/images/pp/").'/'.$user->pp : asset("fashiop/img/blog/author.png")}}"
                                     alt="" width="120" height="120" onclick="openModal()">
                                <div id="myModal" class="modal" style="opacity: 0.95 !important;">
                                    <span class="close2 cursor" onclick="closeModal()">&times;</span>
                                    <div class="modal-content-2">
                                        <img src="{{ $user->pp ? asset("storage/images/pp/").'/'.$user->pp : asset("fashiop/img/blog/author.png")}}"
                                             alt="" style="opacity: 1;">
                                    </div>
                                </div>
                                @if(Request::routeIs('user'))
                                    <form method="POST" enctype="multipart/form-data" style="margin-top: 16px;"
                                          class="updatepp" onchange="$('.updatepp').submit();">
                                        @csrf
                                        <label class="btnImgUpload">
                                            <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"
                                               onclick="document.getElementById('selectedFile').click();"></i>
                                        </label>
                                        <input type="file" name="pp" id="selectedFile" style="display: none;"/>
                                    </form>
                                    <div id="message" style="display: none; margin-top: 16px"></div>
                                @endif
                            </div>
                            <h4>{{ $user->name }}</h4>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget post_category_widget">
                            <ul class="list cat-list">
                                <li {{ Route::current()->getName() == 'user' ? 'style=border-color:#F37320;' : '' }}>
                                    <a href="{{ route('user') }}"
                                       class="d-flex justify-content-between" {{ Route::current()->getName() == 'user' ? 'style=color:#F37320;' : '' }}>
                                        <p>Profile</p>
                                    </a>
                                </li>
                                <li {{ Route::current()->getName() == 'user-jasa' ? 'style=border-color:#F37320;' : '' }}>
                                    <a href="{{ route('user-jasa') }}"
                                       class="d-flex justify-content-between" {{ Route::current()->getName() == 'user-jasa' ? 'style=color:#F37320;' : '' }}>
                                        <p>Jasa-Ku</p>
                                    </a>
                                </li>
                                <li {{ Route::current()->getName() == 'user-order' ? 'style=border-color:#F37320;' : '' }}>
                                    <a href="{{ route('user-order') }}"
                                       class="d-flex justify-content-between" {{ Route::current()->getName() == 'user-order' ? 'style=color:#F37320;' : '' }}>
                                        <p>Pesanan Jasa</p>
                                    </a>
                                </li>
                                <li {{ Route::current()->getName() == 'hero.index' ? 'style=border-color:#F37320;' : '' }}>
                                    <a href="{{ route('hero.index') }}"
                                       class="d-flex justify-content-between" {{ Route::current()->getName() == 'hero.index' ? 'style=color:#F37320;' : '' }}>
                                        <p>Bantuin Hero</p>
                                    </a>
                                </li>
                            </ul>
                            <div class="br"></div>
                        </aside>
                    </div>
                </div>
                @yield('isi')
            </div>
        </div>
    </section>
    <script type="text/javascript">
            // Open the Modal
            function openModal() {
                document.getElementById('myModal').style.display = "block";
            }

            // Close the Modal
            function closeModal() {
                document.getElementById('myModal').style.display = "none";
            }
    </script>
@endsection