<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlamatToJasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jasa', function (Blueprint $table) {
            //
            $table->string('provinsi');
            $table->string('kabkota');
            $table->string('kecamatan');
            $table->string('desa');
            $table->string('alamat');
            $table->string('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jasa', function (Blueprint $table) {
            //
            $table->dropColumn('provinsi');
            $table->dropColumn('kabkota');
            $table->dropColumn('kecamatan');
            $table->dropColumn('desa');
            $table->dropColumn('alamat');
            $table->dropColumn('image');
        });
    }
}
