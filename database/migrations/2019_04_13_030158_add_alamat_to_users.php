<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlamatToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('provinsi')->nullable();
            $table->string('kabkota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('desa')->nullable();
            $table->string('alamat')->nullable();
            $table->string('noidentitas')->nullable()->unique();
            $table->string('fotoidentitas')->nullable();
            $table->string('statusidentitas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('provinsi');
            $table->dropColumn('kabkota');
            $table->dropColumn('kecamatan');
            $table->dropColumn('desa');
            $table->dropColumn('alamat');
            $table->dropColumn('noidentitas');
            $table->dropColumn('fotoidentitas');
            $table->dropColumn('statusidentitas');
        });
    }
}
