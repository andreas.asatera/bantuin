<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingToRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ratings', function (Blueprint $table) {
            //
            $table->string('order_id');
            $table->string('user_id');
            $table->string('status');
            $table->string('komplain')->nullable();
            $table->string('review')->nullable();
            $table->integer('rating')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            //
            $table->dropColumn('order_id');
            $table->dropColumn('user_id');
            $table->dropColumn('status');
            $table->dropColumn('komplain');
            $table->dropColumn('review');
            $table->dropColumn('rating');
        });
    }
}
